# Luly's -ERP

Proyecto gestión comercial 

Gestión de productos ( Insetar, Eliminar, Visualizar, Actualizar)
Gestión de usuarios(  Insetar, Eliminar, Visualizar, Actualizar)
Factura de venta
Control de stock

# Descripción

El sistema de Gestión Comercial Luly’s será un producto diseñado para trabajar en entornos WEB, lo que permitirá su utilización de forma rápida y que permitirá tanto la venta, inventario y pedido de productos.

# Funcionalidad

El sistema podrá listar, registrar, actualizar y eliminar los productos almacenados. Además, el sistema debe listar aquellos productos que estén por agotarse para que el propietario pueda generar un pedido de los productos que él considere. Por otro lado, al momento de realizar una venta, el sistema será capaz de generar una factura y realizar la respectiva reducción del stock en el inventario de productos.

# Universidad Técnica del Norte

Sistema desarrollado por estudiantes de la UTN

- Juan Iza - jdizaf@utn.edu.ec
- Johnn Ramirez -  jeramirezf@utn.edu.ec
- Lenin Tréboles - litrebolesq@utn.edu.ec 
- Brayan Castillo - bacastilloa@utn.edu.ec




