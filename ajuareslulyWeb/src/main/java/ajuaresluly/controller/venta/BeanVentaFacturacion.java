package ajuaresluly.controller.venta;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import ajuaresluly.model.venta.managers.ManagerVenta;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.controller.seguridades.BeanSegLogin;
import minimarketdemo.model.core.entities.FactCliente;
import minimarketdemo.model.core.entities.FactVentaCabecera;
import minimarketdemo.model.core.entities.FactVentaDetalle;
import minimarketdemo.model.core.entities.InvProducto;
import minimarketdemo.model.core.entities.SegUsuario;
import minimarketdemo.model.seguridades.managers.ManagerSeguridades;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanVentaFacturacion implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerVenta mVenta;

	private List<InvProducto> listaProductos;
	private List<FactCliente> listaClientes;
	private List<SegUsuario> listaUsuarios;

	// datos de cabecera
	private String cedulaSeleccionada;
	private Date fecha;
	private SegUsuario usuario;
	private int idSegUsuarioSeleccionado;
	private double subtotal;
	private double iva;
	private double costoTotal;

	// datos de detalle
	private int cantidad;
	private InvProducto idProductoSeleccionado;
	private int idProducto;
	private List<FactVentaDetalle> listaDetalles;

	// Datos de ventas ya registradas d
	private List<FactVentaCabecera> listaVentas;

	private FactVentaDetalle detalleSeleccionado;
	
	@Inject
	private BeanSegLogin beanSegLogin;

	@PostConstruct
	public void inicializar() {
		listaProductos = mVenta.findAllProductos();
		listaClientes = mVenta.findAllClientes();
		listaUsuarios = mVenta.findAllUsuarios();
		listaVentas = mVenta.findAllVentas();
		usuario = mVenta.findUserById(beanSegLogin.getLoginDTO().getIdSegUsuario());
	}
	

	public BeanVentaFacturacion() {
		// TODO Auto-generated constructor stub
	}

	public void actionListenerAgregarDetalle() throws Exception {
		listaDetalles = mVenta.agregarDetalle(listaDetalles, idProductoSeleccionado, cantidad);
		subtotal = mVenta.calcularSubtotalVenta(listaDetalles);
		iva = mVenta.calcularIvaVenta(subtotal);
		costoTotal = mVenta.totalDetalleVenta(subtotal, iva);
	}

	public void listenerSeleccionarProducto() throws Exception {
		idProductoSeleccionado = mVenta.findProductoById(idProducto);
	}

	public void actionListenerGuardar() {
		try {
			idSegUsuarioSeleccionado = usuario.getIdSegUsuario();
			mVenta.guardarVenta(cedulaSeleccionada, fecha, idSegUsuarioSeleccionado, subtotal, iva, costoTotal,
					listaDetalles);
			mVenta.actualizarStock(listaDetalles);
			listaVentas = mVenta.findAllVentas();
			listaDetalles = new ArrayList<FactVentaDetalle>();
			idProductoSeleccionado = new InvProducto();
			JSFUtil.crearMensajeINFO("venta ingresada correctamente");
			costoTotal = 0;
			cedulaSeleccionada = "";
			cantidad = 0;
			fecha = null;
			idSegUsuarioSeleccionado = 0;
			subtotal = 0;
			iva = 0;
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarDetalleTemporal() {
		listaDetalles.remove(detalleSeleccionado);
		detalleSeleccionado = null;
		subtotal = mVenta.calcularSubtotalVenta(listaDetalles);
		iva = mVenta.calcularIvaVenta(subtotal);
		costoTotal = mVenta.totalDetalleVenta(subtotal, iva);
	}

	// metodo jasper
	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("venta/empleado/reporteventas.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/jeramirezf", "jeramirezf", "1003136569");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
		return "";

	}



	public List<InvProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<InvProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public List<FactCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<FactCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public List<SegUsuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<SegUsuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public String getCedulaSeleccionada() {
		return cedulaSeleccionada;
	}

	public void setCedulaSeleccionada(String cedulaSeleccionada) {
		this.cedulaSeleccionada = cedulaSeleccionada;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getIdSegUsuarioSeleccionado() {
		return idSegUsuarioSeleccionado;
	}

	public void setIdSegUsuarioSeleccionado(int idSegUsuarioSeleccionado) {
		this.idSegUsuarioSeleccionado = idSegUsuarioSeleccionado;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public List<FactVentaDetalle> getListaDetalles() {
		return listaDetalles;
	}

	public void setListaDetalles(List<FactVentaDetalle> listaDetalles) {
		this.listaDetalles = listaDetalles;
	}

	public double getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(double costoTotal) {
		this.costoTotal = costoTotal;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public InvProducto getIdProductoSeleccionado() {
		return idProductoSeleccionado;
	}

	public void setIdProductoSeleccionado(InvProducto idProductoSeleccionado) {
		this.idProductoSeleccionado = idProductoSeleccionado;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public List<FactVentaCabecera> getListaVentas() {
		return listaVentas;
	}

	public void setListaVentas(List<FactVentaCabecera> listaVentas) {
		this.listaVentas = listaVentas;
	}

	public FactVentaDetalle getDetalleSeleccionado() {
		return detalleSeleccionado;
	}

	public void setDetalleSeleccionado(FactVentaDetalle detalleSeleccionado) {
		this.detalleSeleccionado = detalleSeleccionado;
	}

	public BeanSegLogin getBeanSegLogin() {
		return beanSegLogin;
	}

	public void setBeanSegLogin(BeanSegLogin beanSegLogin) {
		this.beanSegLogin = beanSegLogin;
	}


	public SegUsuario getUsuario() {
		return usuario;
	}


	public void setUsuario(SegUsuario usuario) {
		this.usuario = usuario;
	}

	

	
}
