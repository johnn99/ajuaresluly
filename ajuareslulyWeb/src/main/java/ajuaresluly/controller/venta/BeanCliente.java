package ajuaresluly.controller.venta;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import ajuaresluly.model.venta.managers.ManagerCliente;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.FactCliente;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanCliente implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerCliente managerCliente;
	private List<FactCliente> listaCliente;
	private String cedula;
	private String nombre;
	private String apellidos;
	private String telefono;
	private String direccion;
	private FactCliente clienteSeleccionado;
	
	public BeanCliente() {
		
	}
	
	@PostConstruct
	public void inicializar() {
		listaCliente=managerCliente.findAllCliente();
	}
	
	
	public void consultarCliente() {
		listaCliente=managerCliente.findAllCliente();
	}
	
	public void actionListenerInsertarCliente() {
		try {
			managerCliente.guardarCliente(cedula, nombre, apellidos, telefono, direccion, true);
			listaCliente=managerCliente.findAllCliente();
			JSFUtil.crearMensajeINFO("Cliente insertado correctamente");
			cedula = null;
			nombre = null;
			apellidos = null;
			telefono = null;
			direccion = null;
		}catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarCliente(String cedula) {
		try {
			managerCliente.eliminarCliente(cedula);
			listaCliente=managerCliente.findAllCliente();
			JSFUtil.crearMensajeINFO("Cliente eliminado correctamente");
		}catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerSeleccionarCliente(FactCliente cliente) {
		clienteSeleccionado=cliente;
		
	}
	
	public void actionListenerActualizarCliente() {
		try {
			managerCliente.actualizarCliente(clienteSeleccionado);
			listaCliente=managerCliente.findAllCliente();
			JSFUtil.crearMensajeINFO("Datos actualizados");
			cedula = null;
			nombre = null;
			apellidos = null;
			telefono = null;
			direccion = null;
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	// metodo jasper
		public String actionReporte() {
			Map<String, Object> parametros = new HashMap<String, Object>();
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
			String ruta = servletContext.getRealPath("venta/empleado/reporteclientes.jasper");
			System.out.println(ruta);
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
			response.setContentType("application/pdf");
			try {
				Class.forName("org.postgresql.Driver");
				Connection connection = null;
				connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/jeramirezf", "jeramirezf",
						"1003136569");
				JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
				JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
				context.getApplication().getStateManager().saveView(context);
				System.out.println("reporte generado.");
				context.responseComplete();

			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();

			}
			return "";

		}
	
	public String actionCargarMenuCliente() {
		listaCliente=managerCliente.findAllCliente();
		return "cliente?faces-redirect=true";
	}

	public List<FactCliente> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<FactCliente> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public String getCedula() {
		return cedula;
	}
	
	

	public FactCliente getClienteSeleccionado() {
		return clienteSeleccionado;
	}

	public void setClienteSeleccionado(FactCliente clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	

}
