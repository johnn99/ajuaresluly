package ajuaresluly.controller.pedidos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import ajuaresluly.model.pedidos.managers.ManagerPedido;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.InvProducto;
import minimarketdemo.model.core.entities.PedCompraCabecera;
import minimarketdemo.model.core.entities.PedCompraDetalle;
import minimarketdemo.model.core.entities.PedProveedor;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanPedido implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerPedido mPedido;

	private List<InvProducto> listaProductos;
	private List<PedProveedor> listaProveedores;

	// datos cabecera
	private String rucSeleccionado;
	private Date fecha;
	private double subtotal;
	private double iva;
	private double costoTotal;

	// datos detalle
	private int cantidad;
	private InvProducto idProductoSeleccionado;
	private int idProducto;
	private List<PedCompraDetalle> listaDetalles;

	// datos de pedidos ya registrados
	private List<PedCompraCabecera> listaPedidos;

	private PedCompraDetalle detalleSeleccionado;
	private PedCompraCabecera cabeceraSeleccionado;
	private List<PedCompraDetalle> listaDetalleCabeceraSeleccionada;

	@PostConstruct
	public void inicializar() {
		listaProductos = mPedido.findAllProductos();
		listaProveedores = mPedido.findAllProveedores();
		listaPedidos = mPedido.findAllPedidos();
	}

	public BeanPedido() {
		// TODO Auto-generated constructor stub
	}

	public void actionListenerAgregarDetalle() throws Exception {
		listaDetalles = mPedido.agregarDetalle(listaDetalles, idProductoSeleccionado, cantidad);
		subtotal = mPedido.calcularSubtotalPedido(listaDetalles);
		iva = mPedido.calcularIvaPedido(subtotal);
		costoTotal = mPedido.totalDetallePedido(subtotal, iva);
	}

	public void listenerSeleccionarProducto() throws Exception {
		idProductoSeleccionado = mPedido.findProductoById(idProducto);
	}

	public void actionListenerGuardar() {
		try {
			mPedido.guardarPedido(rucSeleccionado, fecha, subtotal, iva, costoTotal, listaDetalles);
			mPedido.actualizarStock(listaDetalles);
			listaPedidos = mPedido.findAllPedidos();
			listaDetalles = new ArrayList<PedCompraDetalle>();
			idProductoSeleccionado = new InvProducto();
			JSFUtil.crearMensajeINFO("Compra ingresada correctamente - stock actualizado");
			costoTotal = 0;
			rucSeleccionado = "";
			fecha = null;
			cantidad = 0;
			subtotal = 0;
			iva = 0;
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	// metodo jasper
		public String actionReporte() {
			Map<String, Object> parametros = new HashMap<String, Object>();
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
			String ruta = servletContext.getRealPath("pedido/gerente/reportecompras1.jasper");
			System.out.println(ruta);
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
			response.setContentType("application/pdf");
			try {
				Class.forName("org.postgresql.Driver");
				Connection connection = null;
				connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/jeramirezf", "jeramirezf", "1003136569");
				JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
				JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
				context.getApplication().getStateManager().saveView(context);
				System.out.println("reporte generado.");
				context.responseComplete();

			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();

			}
			return "";

		}

	public void eliminarDetalleTemporal() {
		listaDetalles.remove(detalleSeleccionado);
		detalleSeleccionado = null;
		subtotal = mPedido.calcularSubtotalPedido(listaDetalles);
		iva = mPedido.calcularIvaPedido(subtotal);
		costoTotal = mPedido.totalDetallePedido(subtotal, iva);
	}

	public void actionMostrarDetalleCabecera() throws Exception {
		listaDetalleCabeceraSeleccionada = mPedido.findDetallleByCabeceraId(cabeceraSeleccionado);
	}

	public List<InvProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<InvProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public List<PedProveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<PedProveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public String getRucSeleccionado() {
		return rucSeleccionado;
	}

	public void setRucSeleccionado(String rucSeleccionado) {
		this.rucSeleccionado = rucSeleccionado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(double costoTotal) {
		this.costoTotal = costoTotal;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public InvProducto getIdProductoSeleccionado() {
		return idProductoSeleccionado;
	}

	public void setIdProductoSeleccionado(InvProducto idProductoSeleccionado) {
		this.idProductoSeleccionado = idProductoSeleccionado;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public List<PedCompraDetalle> getListaDetalles() {
		return listaDetalles;
	}

	public void setListaDetalles(List<PedCompraDetalle> listaDetalles) {
		this.listaDetalles = listaDetalles;
	}

	public List<PedCompraCabecera> getListaPedidos() {
		return listaPedidos;
	}

	public void setListaPedidos(List<PedCompraCabecera> listaPedidos) {
		this.listaPedidos = listaPedidos;
	}

	public PedCompraDetalle getDetalleSeleccionado() {
		return detalleSeleccionado;
	}

	public void setDetalleSeleccionado(PedCompraDetalle detalleSeleccionado) {
		this.detalleSeleccionado = detalleSeleccionado;
	}

	public PedCompraCabecera getCabeceraSeleccionado() {
		return cabeceraSeleccionado;
	}

	public void setCabeceraSeleccionado(PedCompraCabecera cabeceraSeleccionado) {
		this.cabeceraSeleccionado = cabeceraSeleccionado;
	}

	public List<PedCompraDetalle> getListaDetalleCabeceraSeleccionada() {
		return listaDetalleCabeceraSeleccionada;
	}

	public void setListaDetalleCabeceraSeleccionada(List<PedCompraDetalle> listaDetalleCabeceraSeleccionada) {
		this.listaDetalleCabeceraSeleccionada = listaDetalleCabeceraSeleccionada;
	}

}
