package ajuaresluly.controller.pedidos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import ajuaresluly.model.pedidos.managers.ManagerProveedor;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.PedProveedor;
import minimarketdemo.model.core.utils.ModelUtil;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanProveedor implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerProveedor managerProveedor;
	private List<PedProveedor> listaProveedor;
	private String ruc;
	private String nombreEmpresa;
	private String telefono;
	private String direccion;
	
	private PedProveedor proveedorSeleccionado;

	public BeanProveedor() {

	}

	@PostConstruct
	public void inicializar() {
		listaProveedor = managerProveedor.findAllProveedor();
	}

	public void consultarProveedor() {
		listaProveedor = managerProveedor.findAllProveedor();
	}

	public void actionListenerInsertarProveedor() {

		try {
			managerProveedor.guardarProveedor(ruc, nombreEmpresa, telefono, direccion, true);
			listaProveedor = managerProveedor.findAllProveedor();
			JSFUtil.crearMensajeINFO("Datos del proveedor insertados.");
			ruc = null;
			nombreEmpresa = null;
			telefono = null;
			direccion = null;
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

	}

	public void actionListenerEliminarProveedor(String ruc) {
		try {
			managerProveedor.eliminarProveedor(ruc);
			listaProveedor = managerProveedor.findAllProveedor();
			JSFUtil.crearMensajeINFO("Eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

	}

	public String actionCargarMenuProveedor() {
		listaProveedor = managerProveedor.findAllProveedor();
		return "proveedor?faces-redirect=true";
	}
	
	public void actionListenerSeleccionarProveedor(PedProveedor proveedor) {
		proveedorSeleccionado=proveedor;
	}
	
	public void actionListenerActualizarProveedor() {
		try {
			managerProveedor.actualizarProveedor(proveedorSeleccionado);
			listaProveedor=managerProveedor.findAllProveedor();
			proveedorSeleccionado = new PedProveedor();
			JSFUtil.crearMensajeINFO("Datos actualizados.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	// metodo jasper
		public String actionReporte() {
			Map<String, Object> parametros = new HashMap<String, Object>();
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
			String ruta = servletContext.getRealPath("pedido/gerente/reporteproveedores.jasper");
			System.out.println(ruta);
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
			response.setContentType("application/pdf");
			try {
				Class.forName("org.postgresql.Driver");
				Connection connection = null;
				connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/jeramirezf", "jeramirezf",
						"1003136569");
				JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
				JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
				context.getApplication().getStateManager().saveView(context);
				System.out.println("reporte generado.");
				context.responseComplete();

			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();

			}
			return "";

		}

	public List<PedProveedor> getListaProveedor() {
		return listaProveedor;
	}

	public void setListaProveedor(List<PedProveedor> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	
	public PedProveedor getProveedorSeleccionado() {
		return proveedorSeleccionado;
	}

	public void setProveedorSeleccionado(PedProveedor proveedorSeleccionado) {
		this.proveedorSeleccionado = proveedorSeleccionado;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

}
