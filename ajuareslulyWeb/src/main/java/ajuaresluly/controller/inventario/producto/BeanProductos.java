package ajuaresluly.controller.inventario.producto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntPredicate;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import ajuaresluly.model.inventario.productos.managers.ManagerProducto;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.InvProducto;
import minimarketdemo.model.core.entities.InvTalla;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanProductos implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerProducto mProducto;
	private List<InvTalla> listaTallas;
	private List<InvProducto> listaProductos;

	private String nombre;
	private String descripcion;
	private double precio;
	private int cantidad;
	private double pvpublico;
	private boolean estado;
	private InvTalla talla;
	private int idTalla;
	private InvProducto productoSeleccionado;
	private InvProducto productos;

	@PostConstruct
	public void inicializar() {
		listaProductos = mProducto.findAllProducto();
		listaTallas = mProducto.findAllTallaProducto();
	}

	public void consultarProducto() {
		listaProductos = mProducto.findAllProducto();
	}

	public void actionListenerInsertarProducto() {
		try {
			mProducto.ingresarNuevoProducto(nombre, descripcion, precio, cantidad, pvpublico, idTalla);
			listaProductos = mProducto.findAllProducto();
			JSFUtil.crearMensajeINFO("Producto ingresado correctamente");
			nombre = null;
			descripcion = null;
			precio = 0;
			cantidad = 0;
			pvpublico = 0;
			idTalla = 0;
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/*
	 * public void actionListenerInsertarProducto() { try {
	 * mProducto.ingresarNuevoProducto(productos, idTalla); productos = new
	 * InvProducto(); listaProductos=mProducto.findAllProducto();
	 * JSFUtil.crearMensajeINFO("Producto ingresado correctamente"); } catch
	 * (Exception e) { JSFUtil.crearMensajeERROR(e.getMessage());
	 * e.printStackTrace(); } }
	 */
	public void actionListenerEliminarProducto(int id_inv_producto) {
		try {
			mProducto.eliminarProducto(id_inv_producto);
			listaProductos = mProducto.findAllProducto();
			JSFUtil.crearMensajeINFO("Prodcuto eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerSeleccionarProducto(InvProducto producto) {
		productoSeleccionado = producto;

	}

	// metodo jasper
	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("inventario/productos/reporteproductos.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/jeramirezf", "jeramirezf",
					"1003136569");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
		return "";

	}

	public void actionListenerActualizarProducto() {
		try {
			mProducto.actualizarProducto(productoSeleccionado, idTalla);
			listaProductos = mProducto.findAllProducto();
			productoSeleccionado = new InvProducto();
			JSFUtil.crearMensajeINFO("Datos actualizados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public String actionCargarMenuProducto() {
		listaProductos = mProducto.findAllProducto();
		return "menu?faces-redirect=true";
	}

	public List<InvProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<InvProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPvpublico() {
		return pvpublico;
	}

	public void setPvpublico(double pvpublico) {
		this.pvpublico = pvpublico;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public InvTalla getTalla() {
		return talla;
	}

	public void setTalla(InvTalla talla) {
		this.talla = talla;
	}

	public InvProducto getProductoSeleccionado() {
		return productoSeleccionado;
	}

	public void setProductoSeleccionado(InvProducto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}

	public int getIdTalla() {
		return idTalla;
	}

	public void setIdTalla(int idTalla) {
		this.idTalla = idTalla;
	}

	public List<InvTalla> getListaTallas() {
		return listaTallas;
	}

	public void setListaTallas(List<InvTalla> listaTallas) {
		this.listaTallas = listaTallas;
	}

	public InvProducto getProductos() {
		return productos;
	}

	public void setProductos(InvProducto productos) {
		this.productos = productos;
	}

}
