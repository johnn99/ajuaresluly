--
-- PostgreSQL database dump
--

-- Dumped from database version 10.19
-- Dumped by pg_dump version 13.6

-- Started on 2022-07-25 22:18:12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 208 (class 1259 OID 191275)
-- Name: aud_bitacora_id_aud_bitacora_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.aud_bitacora_id_aud_bitacora_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 209 (class 1259 OID 191277)
-- Name: aud_bitacora; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.aud_bitacora (
    id_aud_bitacora integer DEFAULT nextval('public.aud_bitacora_id_aud_bitacora_seq'::regclass) NOT NULL,
    fecha_evento timestamp without time zone NOT NULL,
    nombre_clase character varying(100) NOT NULL,
    nombre_metodo character varying(100) NOT NULL,
    descripcion_evento character varying(300) NOT NULL,
    id_usuario character varying(100) NOT NULL,
    direccion_ip character varying(100) NOT NULL
);


--
-- TOC entry 229 (class 1259 OID 191368)
-- Name: fact_cliente; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fact_cliente (
    cedula character varying(13) NOT NULL,
    nombre character varying(20) NOT NULL,
    apellidos character varying(20) NOT NULL,
    telefono character varying(10),
    direccion character varying(70),
    estado boolean NOT NULL
);


--
-- TOC entry 228 (class 1259 OID 191362)
-- Name: fact_venta_cabecera; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fact_venta_cabecera (
    id_fact_venta integer NOT NULL,
    fecha date NOT NULL,
    costo_total numeric(7,2) NOT NULL,
    cedula character varying(13) NOT NULL,
    id_seg_usuario integer NOT NULL,
    iva numeric(7,2) NOT NULL,
    subtotal numeric(7,2) NOT NULL
);


--
-- TOC entry 227 (class 1259 OID 191360)
-- Name: fact_venta_cabecera_id_fact_venta_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fact_venta_cabecera_id_fact_venta_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2459 (class 0 OID 0)
-- Dependencies: 227
-- Name: fact_venta_cabecera_id_fact_venta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fact_venta_cabecera_id_fact_venta_seq OWNED BY public.fact_venta_cabecera.id_fact_venta;


--
-- TOC entry 231 (class 1259 OID 191375)
-- Name: fact_venta_detalle; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fact_venta_detalle (
    id_fact_venta_detalle integer NOT NULL,
    cantidad integer NOT NULL,
    id_fact_venta integer NOT NULL,
    id_inv_producto integer NOT NULL,
    precio_total numeric(7,2) NOT NULL
);


--
-- TOC entry 230 (class 1259 OID 191373)
-- Name: fact_venta_detalle_id_fact_venta_detalle_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fact_venta_detalle_id_fact_venta_detalle_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2460 (class 0 OID 0)
-- Dependencies: 230
-- Name: fact_venta_detalle_id_fact_venta_detalle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fact_venta_detalle_id_fact_venta_detalle_seq OWNED BY public.fact_venta_detalle.id_fact_venta_detalle;


--
-- TOC entry 233 (class 1259 OID 191383)
-- Name: inv_producto; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.inv_producto (
    id_inv_producto integer NOT NULL,
    nombre_producto character varying(50) NOT NULL,
    descripcion_producto character varying(100) NOT NULL,
    precio numeric(7,2) NOT NULL,
    cantidad integer NOT NULL,
    pv_publico numeric(7,2) NOT NULL,
    estado boolean NOT NULL,
    id_inv_talla integer NOT NULL
);


--
-- TOC entry 232 (class 1259 OID 191381)
-- Name: inv_producto_id_inv_producto_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.inv_producto_id_inv_producto_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2461 (class 0 OID 0)
-- Dependencies: 232
-- Name: inv_producto_id_inv_producto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.inv_producto_id_inv_producto_seq OWNED BY public.inv_producto.id_inv_producto;


--
-- TOC entry 235 (class 1259 OID 191391)
-- Name: inv_talla; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.inv_talla (
    id_inv_talla integer NOT NULL,
    nombre_talla character varying(10) NOT NULL
);


--
-- TOC entry 234 (class 1259 OID 191389)
-- Name: inv_talla_id_inv_talla_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.inv_talla_id_inv_talla_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2462 (class 0 OID 0)
-- Dependencies: 234
-- Name: inv_talla_id_inv_talla_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.inv_talla_id_inv_talla_seq OWNED BY public.inv_talla.id_inv_talla;


--
-- TOC entry 239 (class 1259 OID 191407)
-- Name: ped_compra_cabecera; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ped_compra_cabecera (
    id_ped_compra_cabecera integer NOT NULL,
    fecha date NOT NULL,
    costo_total numeric(7,2) NOT NULL,
    ruc character varying(13) NOT NULL,
    iva numeric(7,2) NOT NULL,
    subtotal numeric(7,2) NOT NULL
);


--
-- TOC entry 238 (class 1259 OID 191405)
-- Name: ped_compra_cabecera_id_ped_compra_cabecera_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ped_compra_cabecera_id_ped_compra_cabecera_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2463 (class 0 OID 0)
-- Dependencies: 238
-- Name: ped_compra_cabecera_id_ped_compra_cabecera_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ped_compra_cabecera_id_ped_compra_cabecera_seq OWNED BY public.ped_compra_cabecera.id_ped_compra_cabecera;


--
-- TOC entry 237 (class 1259 OID 191399)
-- Name: ped_compra_detalle; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ped_compra_detalle (
    id_ped_compra_detalle integer NOT NULL,
    cantidad integer NOT NULL,
    id_ped_compra_cabecera integer NOT NULL,
    id_inv_producto integer NOT NULL,
    precio_total numeric(7,2) NOT NULL
);


--
-- TOC entry 236 (class 1259 OID 191397)
-- Name: ped_compra_detalle_id_ped_compra_detalle_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ped_compra_detalle_id_ped_compra_detalle_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2464 (class 0 OID 0)
-- Dependencies: 236
-- Name: ped_compra_detalle_id_ped_compra_detalle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ped_compra_detalle_id_ped_compra_detalle_seq OWNED BY public.ped_compra_detalle.id_ped_compra_detalle;


--
-- TOC entry 240 (class 1259 OID 191413)
-- Name: ped_proveedor; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ped_proveedor (
    ruc character varying(13) NOT NULL,
    empresa character varying(30) NOT NULL,
    telefono character varying(10) NOT NULL,
    direccion character varying(80) NOT NULL,
    estado boolean NOT NULL
);


--
-- TOC entry 218 (class 1259 OID 191322)
-- Name: pry_proyecto_id_pry_proyecto_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pry_proyecto_id_pry_proyecto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 219 (class 1259 OID 191324)
-- Name: pry_proyecto; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pry_proyecto (
    id_pry_proyecto integer DEFAULT nextval('public.pry_proyecto_id_pry_proyecto_seq'::regclass) NOT NULL,
    nombre character varying(100) NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date NOT NULL,
    estado character varying(1) NOT NULL,
    avance smallint NOT NULL
);


--
-- TOC entry 220 (class 1259 OID 191330)
-- Name: pry_tarea_id_pry_tarea_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pry_tarea_id_pry_tarea_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 221 (class 1259 OID 191332)
-- Name: pry_tarea; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pry_tarea (
    id_pry_tarea integer DEFAULT nextval('public.pry_tarea_id_pry_tarea_seq'::regclass) NOT NULL,
    nombre character varying(100) NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date NOT NULL,
    avance smallint NOT NULL,
    id_seg_usuario integer NOT NULL,
    id_pry_proyecto integer
);


--
-- TOC entry 206 (class 1259 OID 191265)
-- Name: seg_asignacion_id_seg_asignacion_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seg_asignacion_id_seg_asignacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 207 (class 1259 OID 191267)
-- Name: seg_asignacion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seg_asignacion (
    id_seg_asignacion integer DEFAULT nextval('public.seg_asignacion_id_seg_asignacion_seq'::regclass) NOT NULL,
    id_seg_usuario integer NOT NULL,
    id_seg_perfil integer NOT NULL
);


--
-- TOC entry 204 (class 1259 OID 191255)
-- Name: seg_modulo_id_seg_modulo_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seg_modulo_id_seg_modulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 205 (class 1259 OID 191257)
-- Name: seg_modulo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seg_modulo (
    id_seg_modulo integer DEFAULT nextval('public.seg_modulo_id_seg_modulo_seq'::regclass) NOT NULL,
    nombre_modulo character varying(50) NOT NULL,
    icono character varying(100)
);


--
-- TOC entry 223 (class 1259 OID 191340)
-- Name: seg_perfil; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seg_perfil (
    id_seg_perfil integer NOT NULL,
    nombre_perfil character varying(50) NOT NULL,
    ruta_acceso character varying(100) NOT NULL,
    id_seg_modulo integer NOT NULL
);


--
-- TOC entry 222 (class 1259 OID 191338)
-- Name: seg_perfil_id_seg_perfil_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seg_perfil_id_seg_perfil_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2465 (class 0 OID 0)
-- Dependencies: 222
-- Name: seg_perfil_id_seg_perfil_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.seg_perfil_id_seg_perfil_seq OWNED BY public.seg_perfil.id_seg_perfil;


--
-- TOC entry 202 (class 1259 OID 191247)
-- Name: seg_usuario_id_seg_usuario_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seg_usuario_id_seg_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 203 (class 1259 OID 191249)
-- Name: seg_usuario; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seg_usuario (
    id_seg_usuario integer DEFAULT nextval('public.seg_usuario_id_seg_usuario_seq'::regclass) NOT NULL,
    codigo character varying(10) NOT NULL,
    apellidos character varying(50) NOT NULL,
    nombres character varying(50) NOT NULL,
    correo character varying(50) NOT NULL,
    clave character varying(50) NOT NULL,
    activo boolean NOT NULL
);


--
-- TOC entry 210 (class 1259 OID 191286)
-- Name: thm_cargo_id_thm_cargo_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thm_cargo_id_thm_cargo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 211 (class 1259 OID 191288)
-- Name: thm_cargo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thm_cargo (
    id_thm_cargo integer DEFAULT nextval('public.thm_cargo_id_thm_cargo_seq'::regclass) NOT NULL,
    nombre_cargo character varying(50) NOT NULL,
    remuneracion_mensual numeric(7,2) NOT NULL
);


--
-- TOC entry 212 (class 1259 OID 191296)
-- Name: thm_empleado_id_thm_empleado_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thm_empleado_id_thm_empleado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 213 (class 1259 OID 191298)
-- Name: thm_empleado; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thm_empleado (
    id_thm_empleado integer DEFAULT nextval('public.thm_empleado_id_thm_empleado_seq'::regclass) NOT NULL,
    id_thm_cargo integer NOT NULL,
    id_seg_usuario integer NOT NULL,
    horas_trabajadas smallint NOT NULL,
    horas_extra smallint NOT NULL,
    cuota_prestamo numeric(7,2) NOT NULL
);


--
-- TOC entry 225 (class 1259 OID 191348)
-- Name: thm_periodo_rol; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thm_periodo_rol (
    id_thm_periodo_rol integer NOT NULL,
    nombre_periodo_rol character varying(7) NOT NULL,
    generado boolean NOT NULL
);


--
-- TOC entry 224 (class 1259 OID 191346)
-- Name: thm_periodo_rol_id_thm_periodo_rol_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thm_periodo_rol_id_thm_periodo_rol_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2466 (class 0 OID 0)
-- Dependencies: 224
-- Name: thm_periodo_rol_id_thm_periodo_rol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.thm_periodo_rol_id_thm_periodo_rol_seq OWNED BY public.thm_periodo_rol.id_thm_periodo_rol;


--
-- TOC entry 214 (class 1259 OID 191306)
-- Name: thm_rol_cabecera_id_thm_rol_cabecera_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thm_rol_cabecera_id_thm_rol_cabecera_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 215 (class 1259 OID 191308)
-- Name: thm_rol_cabecera; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thm_rol_cabecera (
    id_thm_rol_cabecera integer DEFAULT nextval('public.thm_rol_cabecera_id_thm_rol_cabecera_seq'::regclass) NOT NULL,
    id_thm_empleado integer NOT NULL,
    id_thm_periodo_rol integer NOT NULL,
    nombre_cargo character varying(50) NOT NULL,
    horas_trabajadas smallint NOT NULL,
    horas_extras smallint NOT NULL,
    subtotal_ingresos numeric(7,2) NOT NULL,
    subtotal_ingresos_adicionales numeric(7,2) NOT NULL,
    subtotal_egresos numeric(7,2) NOT NULL,
    total numeric(7,2) NOT NULL
);


--
-- TOC entry 216 (class 1259 OID 191314)
-- Name: thm_rol_detalle_id_thm_rol_detalle_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.thm_rol_detalle_id_thm_rol_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


--
-- TOC entry 217 (class 1259 OID 191316)
-- Name: thm_rol_detalle; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.thm_rol_detalle (
    id_thm_rol_detalle integer DEFAULT nextval('public.thm_rol_detalle_id_thm_rol_detalle_seq'::regclass) NOT NULL,
    id_thm_rol_cabecera integer NOT NULL,
    tipo_detalle character varying(2) NOT NULL,
    descripcion character varying(100) NOT NULL,
    valor numeric(7,2) NOT NULL,
    orden smallint NOT NULL
);


--
-- TOC entry 226 (class 1259 OID 191356)
-- Name: vw_thm_consulta_rol; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.vw_thm_consulta_rol AS
 SELECT trc.id_thm_rol_cabecera,
    tpr.nombre_periodo_rol,
    trc.id_thm_empleado,
    trc.total,
    te.horas_extra,
    su.apellidos
   FROM public.thm_rol_cabecera trc,
    public.thm_periodo_rol tpr,
    public.thm_empleado te,
    public.seg_usuario su
  WHERE ((tpr.id_thm_periodo_rol = trc.id_thm_periodo_rol) AND (trc.id_thm_empleado = te.id_thm_empleado) AND (te.id_seg_usuario = su.id_seg_usuario));


--
-- TOC entry 2220 (class 2604 OID 191365)
-- Name: fact_venta_cabecera id_fact_venta; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fact_venta_cabecera ALTER COLUMN id_fact_venta SET DEFAULT nextval('public.fact_venta_cabecera_id_fact_venta_seq'::regclass);


--
-- TOC entry 2221 (class 2604 OID 191378)
-- Name: fact_venta_detalle id_fact_venta_detalle; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fact_venta_detalle ALTER COLUMN id_fact_venta_detalle SET DEFAULT nextval('public.fact_venta_detalle_id_fact_venta_detalle_seq'::regclass);


--
-- TOC entry 2222 (class 2604 OID 191386)
-- Name: inv_producto id_inv_producto; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inv_producto ALTER COLUMN id_inv_producto SET DEFAULT nextval('public.inv_producto_id_inv_producto_seq'::regclass);


--
-- TOC entry 2223 (class 2604 OID 191394)
-- Name: inv_talla id_inv_talla; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inv_talla ALTER COLUMN id_inv_talla SET DEFAULT nextval('public.inv_talla_id_inv_talla_seq'::regclass);


--
-- TOC entry 2225 (class 2604 OID 191410)
-- Name: ped_compra_cabecera id_ped_compra_cabecera; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ped_compra_cabecera ALTER COLUMN id_ped_compra_cabecera SET DEFAULT nextval('public.ped_compra_cabecera_id_ped_compra_cabecera_seq'::regclass);


--
-- TOC entry 2224 (class 2604 OID 191402)
-- Name: ped_compra_detalle id_ped_compra_detalle; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ped_compra_detalle ALTER COLUMN id_ped_compra_detalle SET DEFAULT nextval('public.ped_compra_detalle_id_ped_compra_detalle_seq'::regclass);


--
-- TOC entry 2218 (class 2604 OID 191343)
-- Name: seg_perfil id_seg_perfil; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_perfil ALTER COLUMN id_seg_perfil SET DEFAULT nextval('public.seg_perfil_id_seg_perfil_seq'::regclass);


--
-- TOC entry 2219 (class 2604 OID 191351)
-- Name: thm_periodo_rol id_thm_periodo_rol; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_periodo_rol ALTER COLUMN id_thm_periodo_rol SET DEFAULT nextval('public.thm_periodo_rol_id_thm_periodo_rol_seq'::regclass);


--
-- TOC entry 2423 (class 0 OID 191277)
-- Dependencies: 209
-- Data for Name: aud_bitacora; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (1, '2022-07-23 21:02:04.013', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (2, '2022-07-23 21:02:51.843', 'ManagerSeguridades', 'login', 'No existe usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (3, '2022-07-23 21:03:01.694', 'ManagerSeguridades', 'login', 'No existe usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (4, '2022-07-23 21:07:43.415', 'ManagerSeguridades', 'login', 'No existe usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (5, '2022-07-23 21:07:47.856', 'ManagerSeguridades', 'login', 'No existe usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (6, '2022-07-23 21:10:17.29', 'ManagerSeguridades', 'login', 'No existe usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (7, '2022-07-23 21:15:41.856', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (8, '2022-07-23 21:15:43.049', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador creado (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (9, '2022-07-23 21:15:43.667', 'ManagerSeguridades', 'inicializarDemo', 'Creado módulo de seguridades (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (10, '2022-07-23 21:15:44.571', 'ManagerSeguridades', 'asignarPerfil', 'Perfil 1 asignado a usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (11, '2022-07-23 21:15:45.354', 'ManagerSeguridades', 'inicializarDemo', 'Creado módulo de auditoría (id : 2)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (12, '2022-07-23 21:15:46.452', 'ManagerSeguridades', 'asignarPerfil', 'Perfil 2 asignado a usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (13, '2022-07-23 21:15:46.741', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (14, '2022-07-23 21:15:51.963', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (15, '2022-07-23 21:15:58.697', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (16, '2022-07-23 21:19:20.358', 'ManagerSeguridades', 'asignarPerfil', 'Perfil 4 asignado a usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (17, '2022-07-23 21:19:29.951', 'ManagerSeguridades', 'asignarPerfil', 'Perfil 5 asignado a usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (18, '2022-07-23 21:19:36.193', 'ManagerSeguridades', 'asignarPerfil', 'Perfil 3 asignado a usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (19, '2022-07-23 21:19:50.296', 'ManagerSeguridades', 'cerrarSesion', 'Cerrar sesión usuario: 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (20, '2022-07-23 21:19:53.861', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (21, '2022-07-23 21:40:50.445', 'ManagerSeguridades', 'cerrarSesion', 'Cerrar sesión usuario: 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (22, '2022-07-23 21:40:52.817', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (23, '2022-07-23 21:41:21.844', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /inventario/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (24, '2022-07-23 21:41:25.074', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (25, '2022-07-23 21:57:03.48', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /inventario/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (26, '2022-07-23 21:57:05.646', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (27, '2022-07-23 22:49:41.763', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /pedido/gerente/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (28, '2022-07-23 22:49:44.111', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (29, '2022-07-23 22:56:26.286', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /pedido/gerente/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (30, '2022-07-23 22:56:30.137', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (31, '2022-07-23 22:57:20.568', 'ManagerSeguridades', 'cerrarSesion', 'Cerrar sesión usuario: 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (32, '2022-07-23 22:57:22.53', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (33, '2022-07-23 23:31:46.759', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /pedido/gerente/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (34, '2022-07-23 23:31:50.611', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (35, '2022-07-23 23:39:49.339', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (36, '2022-07-23 23:39:52.421', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (37, '2022-07-23 23:39:58.177', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (38, '2022-07-23 23:40:00.087', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (39, '2022-07-23 23:40:03.037', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (40, '2022-07-24 11:25:13.231', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (41, '2022-07-24 11:25:14.091', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (42, '2022-07-24 11:25:14.406', 'ManagerSeguridades', 'inicializarDemo', 'Creado mÃ³dulo de auditorÃ­a (id : 6)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (43, '2022-07-24 11:25:14.481', 'ManagerSeguridades', 'asignarPerfil', 'Perfil 6 asignado a usuario 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (44, '2022-07-24 11:25:14.499', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (45, '2022-07-24 11:25:15.787', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (46, '2022-07-24 11:27:10.305', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (47, '2022-07-24 12:09:03.033', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (48, '2022-07-24 12:09:03.179', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (49, '2022-07-24 12:09:03.353', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (50, '2022-07-24 12:09:04.501', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (51, '2022-07-24 12:09:04.85', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (52, '2022-07-24 12:09:07.277', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (53, '2022-07-24 12:09:08.835', 'ManagerSeguridades', 'login', 'No coincide la clave 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (54, '2022-07-24 12:09:12.633', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (55, '2022-07-24 12:09:12.678', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (56, '2022-07-24 12:09:12.85', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (57, '2022-07-24 12:09:15.349', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (58, '2022-07-24 12:10:03.246', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 1 intento no autorizado a /inventario/productos/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (59, '2022-07-24 12:10:06.604', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (60, '2022-07-24 12:11:37.249', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 1 intento no autorizado a /inventario/productos/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (61, '2022-07-24 12:13:02.737', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (62, '2022-07-24 12:13:02.788', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (63, '2022-07-24 12:13:02.987', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (64, '2022-07-24 12:13:04.281', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (65, '2022-07-24 16:15:35.765', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (66, '2022-07-24 22:36:28.607', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (67, '2022-07-24 22:40:45.693', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 1 intento no autorizado a /inventario/productos/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (68, '2022-07-24 22:40:47.65', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (69, '2022-07-24 22:43:23.066', 'ManagerSeguridades', 'cerrarSesion', 'Cerrar sesión usuario: 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (70, '2022-07-24 22:43:25.427', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (71, '2022-07-24 23:19:13.224', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /pedido/gerente/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (72, '2022-07-24 23:19:16.676', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (73, '2022-07-25 00:05:29.129', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /pedido/gerente/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (74, '2022-07-25 00:05:32.764', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (75, '2022-07-25 00:29:53.109', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (76, '2022-07-25 07:38:48.8', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (77, '2022-07-25 10:11:49.973', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (78, '2022-07-25 10:11:53.343', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (79, '2022-07-25 10:12:09.457', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (80, '2022-07-25 10:12:09.741', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (81, '2022-07-25 10:12:09.853', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (82, '2022-07-25 10:12:11.583', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (83, '2022-07-25 11:27:12.068', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (84, '2022-07-25 11:27:16.216', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (85, '2022-07-25 11:52:06.978', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (86, '2022-07-25 11:52:15.32', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (87, '2022-07-25 11:53:19.139', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (88, '2022-07-25 11:53:24.812', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (89, '2022-07-25 12:16:58.915', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (90, '2022-07-25 12:17:03.915', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (91, '2022-07-25 13:46:12.201', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (92, '2022-07-25 19:17:04.08', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (93, '2022-07-25 19:36:35.614', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (94, '2022-07-25 19:36:39.024', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (95, '2022-07-25 19:39:34.317', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (96, '2022-07-25 19:39:34.553', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (97, '2022-07-25 19:39:34.745', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (98, '2022-07-25 19:39:36.029', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (99, '2022-07-25 19:42:14.233', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (100, '2022-07-25 19:42:15.826', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (101, '2022-07-25 19:42:15.874', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (102, '2022-07-25 19:42:16.041', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (103, '2022-07-25 19:42:17.27', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (104, '2022-07-25 19:45:11.376', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (105, '2022-07-25 19:45:11.564', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (106, '2022-07-25 19:45:11.754', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (107, '2022-07-25 19:45:13.339', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (108, '2022-07-25 19:45:24.743', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (109, '2022-07-25 19:45:40.915', 'ManagerSeguridades', 'actualizarUsuario', 'se actualizó al usuario admin', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (110, '2022-07-25 19:45:47.911', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (111, '2022-07-25 19:45:45.855', 'ManagerSeguridades', 'cerrarSesion', 'Cerrar sesión usuario: 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (112, '2022-07-25 19:45:50.127', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (113, '2022-07-25 19:47:59.441', 'ManagerSeguridades', 'cerrarSesion', 'Cerrar sesión usuario: 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (114, '2022-07-25 19:48:01.257', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (115, '2022-07-25 19:48:01.31', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (116, '2022-07-25 19:48:01.481', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (117, '2022-07-25 19:48:02.665', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (118, '2022-07-25 19:49:42.113', 'ManagerSeguridades', 'cerrarSesion', 'Cerrar sesión usuario: 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (119, '2022-07-25 19:49:43.379', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (120, '2022-07-25 19:49:43.428', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (121, '2022-07-25 19:49:43.601', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (122, '2022-07-25 19:49:45.328', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (123, '2022-07-25 19:51:47.14', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (124, '2022-07-25 19:51:47.357', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (125, '2022-07-25 19:51:47.552', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (126, '2022-07-25 19:51:49.334', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (127, '2022-07-25 19:56:56.27', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /pedido/gerente/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (128, '2022-07-25 19:56:58.76', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (129, '2022-07-25 19:59:59.347', 'ManagerSeguridades', 'cerrarSesion', 'Cerrar sesión usuario: 1', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (130, '2022-07-25 20:00:01.42', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (131, '2022-07-25 20:05:21.309', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (132, '2022-07-25 20:05:21.468', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (133, '2022-07-25 20:05:21.649', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (134, '2022-07-25 20:05:22.812', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (135, '2022-07-25 20:09:45.493', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /inventario/productos/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (136, '2022-07-25 20:09:50.86', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (137, '2022-07-25 20:13:31.49', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /pedido/gerente/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (138, '2022-07-25 20:14:01.402', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (139, '2022-07-25 20:27:23.198', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (140, '2022-07-25 20:27:23.359', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (141, '2022-07-25 20:27:23.53', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (142, '2022-07-25 20:27:24.795', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (143, '2022-07-25 20:32:23.502', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (144, '2022-07-25 20:32:23.775', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (145, '2022-07-25 20:32:24.003', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (146, '2022-07-25 20:32:25.321', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (147, '2022-07-25 20:40:15.863', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (148, '2022-07-25 20:40:16.014', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (149, '2022-07-25 20:40:16.2', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (150, '2022-07-25 20:40:17.569', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (151, '2022-07-25 20:44:12.497', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (152, '2022-07-25 20:44:12.53', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (153, '2022-07-25 20:44:12.696', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (154, '2022-07-25 20:44:13.935', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (155, '2022-07-25 20:47:15.657', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (156, '2022-07-25 20:47:15.811', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (157, '2022-07-25 20:47:15.97', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (158, '2022-07-25 20:47:17.187', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (159, '2022-07-25 20:47:30.613', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (160, '2022-07-25 20:54:11.104', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (161, '2022-07-25 20:54:15.051', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (162, '2022-07-25 21:23:06.864', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (163, '2022-07-25 21:23:10.824', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (164, '2022-07-25 21:33:44.782', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (165, '2022-07-25 21:33:56.3', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (166, '2022-07-25 21:49:24.066', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (167, '2022-07-25 21:53:05.118', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (168, '2022-07-25 21:53:05.116', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (169, '2022-07-25 21:53:05.437', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (170, '2022-07-25 21:53:34.551', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (171, '2022-07-25 21:57:26.137', 'ManagerSeguridades', 'accesoNoPermitido', 'Usuario 0 intento no autorizado a /venta/empleado/menu.xhtml', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (172, '2022-07-25 21:57:38.196', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (173, '2022-07-25 21:57:57.866', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (174, '2022-07-25 22:02:36.205', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (175, '2022-07-25 22:05:55.459', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (176, '2022-07-25 22:08:52.791', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (177, '2022-07-25 22:08:53.235', 'ManagerSeguridades', 'inicializarDemo', 'Usuario administrador ya existe (id : 1)', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (178, '2022-07-25 22:08:53.41', 'ManagerSeguridades', 'inicializarDemo', 'Inicializacion de bdd demo terminada.', 'anonimo', 'localhost');
INSERT INTO public.aud_bitacora (id_aud_bitacora, fecha_evento, nombre_clase, nombre_metodo, descripcion_evento, id_usuario, direccion_ip) VALUES (179, '2022-07-25 22:08:54.666', 'ManagerSeguridades', 'login', 'Login exitoso 1', '1', '127.0.0.1');


--
-- TOC entry 2442 (class 0 OID 191368)
-- Dependencies: 229
-- Data for Name: fact_cliente; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.fact_cliente (cedula, nombre, apellidos, telefono, direccion, estado) VALUES ('1002003000', 'Daniel', 'Rodriguez', '987654321', 'Otavalo', true);
INSERT INTO public.fact_cliente (cedula, nombre, apellidos, telefono, direccion, estado) VALUES ('1002003001', 'Lucas', 'Salgado', '987654322', 'Ibarra', true);
INSERT INTO public.fact_cliente (cedula, nombre, apellidos, telefono, direccion, estado) VALUES ('1002003002', 'Andrea', 'Vinueza', '987654323', 'Atuntaqui', true);
INSERT INTO public.fact_cliente (cedula, nombre, apellidos, telefono, direccion, estado) VALUES ('1002003004', 'Sandra', 'Moran', '098765435', 'Quito', true);
INSERT INTO public.fact_cliente (cedula, nombre, apellidos, telefono, direccion, estado) VALUES ('1002003003', 'Mateo', 'Terán', '098765434', 'Cayambe', true);


--
-- TOC entry 2441 (class 0 OID 191362)
-- Dependencies: 228
-- Data for Name: fact_venta_cabecera; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.fact_venta_cabecera (id_fact_venta, fecha, costo_total, cedula, id_seg_usuario, iva, subtotal) VALUES (1, '2022-07-23', 95.20, '1002003000', 1, 10.20, 85.00);
INSERT INTO public.fact_venta_cabecera (id_fact_venta, fecha, costo_total, cedula, id_seg_usuario, iva, subtotal) VALUES (2, '2022-07-23', 56.00, '1002003001', 1, 6.00, 50.00);
INSERT INTO public.fact_venta_cabecera (id_fact_venta, fecha, costo_total, cedula, id_seg_usuario, iva, subtotal) VALUES (3, '2022-07-24', 67.20, '1002003002', 1, 7.20, 60.00);
INSERT INTO public.fact_venta_cabecera (id_fact_venta, fecha, costo_total, cedula, id_seg_usuario, iva, subtotal) VALUES (4, '2022-07-25', 162.40, '1002003001', 1, 17.40, 145.00);


--
-- TOC entry 2444 (class 0 OID 191375)
-- Dependencies: 231
-- Data for Name: fact_venta_detalle; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.fact_venta_detalle (id_fact_venta_detalle, cantidad, id_fact_venta, id_inv_producto, precio_total) VALUES (1, 1, 1, 1, 60.00);
INSERT INTO public.fact_venta_detalle (id_fact_venta_detalle, cantidad, id_fact_venta, id_inv_producto, precio_total) VALUES (2, 1, 1, 2, 25.00);
INSERT INTO public.fact_venta_detalle (id_fact_venta_detalle, cantidad, id_fact_venta, id_inv_producto, precio_total) VALUES (3, 2, 2, 2, 50.00);
INSERT INTO public.fact_venta_detalle (id_fact_venta_detalle, cantidad, id_fact_venta, id_inv_producto, precio_total) VALUES (4, 1, 3, 1, 60.00);
INSERT INTO public.fact_venta_detalle (id_fact_venta_detalle, cantidad, id_fact_venta, id_inv_producto, precio_total) VALUES (5, 2, 4, 1, 120.00);
INSERT INTO public.fact_venta_detalle (id_fact_venta_detalle, cantidad, id_fact_venta, id_inv_producto, precio_total) VALUES (6, 1, 4, 2, 25.00);


--
-- TOC entry 2446 (class 0 OID 191383)
-- Dependencies: 233
-- Data for Name: inv_producto; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.inv_producto (id_inv_producto, nombre_producto, descripcion_producto, precio, cantidad, pv_publico, estado, id_inv_talla) VALUES (1, 'vestido', 'vestido para boda', 30.00, 16, 60.00, true, 1);
INSERT INTO public.inv_producto (id_inv_producto, nombre_producto, descripcion_producto, precio, cantidad, pv_publico, estado, id_inv_talla) VALUES (2, 'camisa', 'camisa blanca', 15.00, 26, 25.00, true, 1);
INSERT INTO public.inv_producto (id_inv_producto, nombre_producto, descripcion_producto, precio, cantidad, pv_publico, estado, id_inv_talla) VALUES (3, 'chaqueta', 'chaqueta negra', 25.00, 15, 33.00, true, 1);


--
-- TOC entry 2448 (class 0 OID 191391)
-- Dependencies: 235
-- Data for Name: inv_talla; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.inv_talla (id_inv_talla, nombre_talla) VALUES (1, 'M');
INSERT INTO public.inv_talla (id_inv_talla, nombre_talla) VALUES (2, 'L');
INSERT INTO public.inv_talla (id_inv_talla, nombre_talla) VALUES (3, 'S');
INSERT INTO public.inv_talla (id_inv_talla, nombre_talla) VALUES (4, 'XL');


--
-- TOC entry 2452 (class 0 OID 191407)
-- Dependencies: 239
-- Data for Name: ped_compra_cabecera; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ped_compra_cabecera (id_ped_compra_cabecera, fecha, costo_total, ruc, iva, subtotal) VALUES (1, '2022-07-23', 252.00, '1234567890001', 27.00, 225.00);
INSERT INTO public.ped_compra_cabecera (id_ped_compra_cabecera, fecha, costo_total, ruc, iva, subtotal) VALUES (2, '2022-07-23', 504.00, '1234567890001', 54.00, 450.00);
INSERT INTO public.ped_compra_cabecera (id_ped_compra_cabecera, fecha, costo_total, ruc, iva, subtotal) VALUES (3, '2022-07-23', 504.00, '1234567890001', 54.00, 450.00);
INSERT INTO public.ped_compra_cabecera (id_ped_compra_cabecera, fecha, costo_total, ruc, iva, subtotal) VALUES (4, '2022-07-25', 140.00, '1029121212121', 15.00, 125.00);


--
-- TOC entry 2450 (class 0 OID 191399)
-- Dependencies: 237
-- Data for Name: ped_compra_detalle; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ped_compra_detalle (id_ped_compra_detalle, cantidad, id_ped_compra_cabecera, id_inv_producto, precio_total) VALUES (1, 6, 1, 1, 180.00);
INSERT INTO public.ped_compra_detalle (id_ped_compra_detalle, cantidad, id_ped_compra_cabecera, id_inv_producto, precio_total) VALUES (2, 3, 1, 2, 45.00);
INSERT INTO public.ped_compra_detalle (id_ped_compra_detalle, cantidad, id_ped_compra_cabecera, id_inv_producto, precio_total) VALUES (3, 10, 2, 1, 300.00);
INSERT INTO public.ped_compra_detalle (id_ped_compra_detalle, cantidad, id_ped_compra_cabecera, id_inv_producto, precio_total) VALUES (4, 10, 2, 2, 150.00);
INSERT INTO public.ped_compra_detalle (id_ped_compra_detalle, cantidad, id_ped_compra_cabecera, id_inv_producto, precio_total) VALUES (5, 10, 3, 1, 300.00);
INSERT INTO public.ped_compra_detalle (id_ped_compra_detalle, cantidad, id_ped_compra_cabecera, id_inv_producto, precio_total) VALUES (6, 10, 3, 2, 150.00);
INSERT INTO public.ped_compra_detalle (id_ped_compra_detalle, cantidad, id_ped_compra_cabecera, id_inv_producto, precio_total) VALUES (7, 5, 4, 3, 125.00);


--
-- TOC entry 2453 (class 0 OID 191413)
-- Dependencies: 240
-- Data for Name: ped_proveedor; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ped_proveedor (ruc, empresa, telefono, direccion, estado) VALUES ('1029121212121', 'Pedrasa', '0951214131', 'Ibarra', true);
INSERT INTO public.ped_proveedor (ruc, empresa, telefono, direccion, estado) VALUES ('1000998881003', 'MiniStyle', '0987654323', 'Quito', true);
INSERT INTO public.ped_proveedor (ruc, empresa, telefono, direccion, estado) VALUES ('1000998881004', 'Cuneli', '0987654324', 'Otavalo', true);
INSERT INTO public.ped_proveedor (ruc, empresa, telefono, direccion, estado) VALUES ('1234567890001', 'Sante', '0987654321', 'Atuntaqui', true);
INSERT INTO public.ped_proveedor (ruc, empresa, telefono, direccion, estado) VALUES ('1234567890002', 'Textiles', '0987654322', 'Quito', true);


--
-- TOC entry 2433 (class 0 OID 191324)
-- Dependencies: 219
-- Data for Name: pry_proyecto; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2435 (class 0 OID 191332)
-- Dependencies: 221
-- Data for Name: pry_tarea; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2421 (class 0 OID 191267)
-- Dependencies: 207
-- Data for Name: seg_asignacion; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.seg_asignacion (id_seg_asignacion, id_seg_usuario, id_seg_perfil) VALUES (1, 1, 1);
INSERT INTO public.seg_asignacion (id_seg_asignacion, id_seg_usuario, id_seg_perfil) VALUES (2, 1, 2);
INSERT INTO public.seg_asignacion (id_seg_asignacion, id_seg_usuario, id_seg_perfil) VALUES (3, 1, 4);
INSERT INTO public.seg_asignacion (id_seg_asignacion, id_seg_usuario, id_seg_perfil) VALUES (4, 1, 5);
INSERT INTO public.seg_asignacion (id_seg_asignacion, id_seg_usuario, id_seg_perfil) VALUES (5, 1, 3);


--
-- TOC entry 2419 (class 0 OID 191257)
-- Dependencies: 205
-- Data for Name: seg_modulo; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.seg_modulo (id_seg_modulo, nombre_modulo, icono) VALUES (1, 'Seguridades', 'pi pi-lock');
INSERT INTO public.seg_modulo (id_seg_modulo, nombre_modulo, icono) VALUES (2, 'Auditoría', 'pi pi-eye');
INSERT INTO public.seg_modulo (id_seg_modulo, nombre_modulo, icono) VALUES (3, 'Venta', 'pi pi-shopping-bag');
INSERT INTO public.seg_modulo (id_seg_modulo, nombre_modulo, icono) VALUES (4, 'Pedido', 'pi pi-plus');
INSERT INTO public.seg_modulo (id_seg_modulo, nombre_modulo, icono) VALUES (5, 'Inventario', 'pi pi-file-o');


--
-- TOC entry 2437 (class 0 OID 191340)
-- Dependencies: 223
-- Data for Name: seg_perfil; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.seg_perfil (id_seg_perfil, nombre_perfil, ruta_acceso, id_seg_modulo) VALUES (1, 'Administrador', 'seguridades/administrador/menu', 1);
INSERT INTO public.seg_perfil (id_seg_perfil, nombre_perfil, ruta_acceso, id_seg_modulo) VALUES (2, 'Auditor', 'auditoria/auditor/menu', 2);
INSERT INTO public.seg_perfil (id_seg_perfil, nombre_perfil, ruta_acceso, id_seg_modulo) VALUES (3, 'Empleado', 'venta/empleado/menu', 3);
INSERT INTO public.seg_perfil (id_seg_perfil, nombre_perfil, ruta_acceso, id_seg_modulo) VALUES (4, 'gerente', 'pedido/gerente/menu', 4);
INSERT INTO public.seg_perfil (id_seg_perfil, nombre_perfil, ruta_acceso, id_seg_modulo) VALUES (5, 'Empleado', 'inventario/productos/menu', 5);


--
-- TOC entry 2417 (class 0 OID 191249)
-- Dependencies: 203
-- Data for Name: seg_usuario; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.seg_usuario (id_seg_usuario, codigo, apellidos, nombres, correo, clave, activo) VALUES (1, 'admin', 'admin', 'admin', 'admin@ajuaresluly.com', 'admin', true);


--
-- TOC entry 2425 (class 0 OID 191288)
-- Dependencies: 211
-- Data for Name: thm_cargo; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.thm_cargo (id_thm_cargo, nombre_cargo, remuneracion_mensual) VALUES (1, 'Director financiero', 1300.00);
INSERT INTO public.thm_cargo (id_thm_cargo, nombre_cargo, remuneracion_mensual) VALUES (2, 'Bodeguero', 890.00);


--
-- TOC entry 2427 (class 0 OID 191298)
-- Dependencies: 213
-- Data for Name: thm_empleado; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2439 (class 0 OID 191348)
-- Dependencies: 225
-- Data for Name: thm_periodo_rol; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.thm_periodo_rol (id_thm_periodo_rol, nombre_periodo_rol, generado) VALUES (1, '2021-10', false);
INSERT INTO public.thm_periodo_rol (id_thm_periodo_rol, nombre_periodo_rol, generado) VALUES (2, '2021-11', false);
INSERT INTO public.thm_periodo_rol (id_thm_periodo_rol, nombre_periodo_rol, generado) VALUES (3, '2021-12', false);
INSERT INTO public.thm_periodo_rol (id_thm_periodo_rol, nombre_periodo_rol, generado) VALUES (4, '2022-01', false);
INSERT INTO public.thm_periodo_rol (id_thm_periodo_rol, nombre_periodo_rol, generado) VALUES (5, '2022-02', false);


--
-- TOC entry 2429 (class 0 OID 191308)
-- Dependencies: 215
-- Data for Name: thm_rol_cabecera; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2431 (class 0 OID 191316)
-- Dependencies: 217
-- Data for Name: thm_rol_detalle; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2467 (class 0 OID 0)
-- Dependencies: 208
-- Name: aud_bitacora_id_aud_bitacora_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.aud_bitacora_id_aud_bitacora_seq', 179, true);


--
-- TOC entry 2468 (class 0 OID 0)
-- Dependencies: 227
-- Name: fact_venta_cabecera_id_fact_venta_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.fact_venta_cabecera_id_fact_venta_seq', 4, true);


--
-- TOC entry 2469 (class 0 OID 0)
-- Dependencies: 230
-- Name: fact_venta_detalle_id_fact_venta_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.fact_venta_detalle_id_fact_venta_detalle_seq', 6, true);


--
-- TOC entry 2470 (class 0 OID 0)
-- Dependencies: 232
-- Name: inv_producto_id_inv_producto_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.inv_producto_id_inv_producto_seq', 3, true);


--
-- TOC entry 2471 (class 0 OID 0)
-- Dependencies: 234
-- Name: inv_talla_id_inv_talla_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.inv_talla_id_inv_talla_seq', 4, true);


--
-- TOC entry 2472 (class 0 OID 0)
-- Dependencies: 238
-- Name: ped_compra_cabecera_id_ped_compra_cabecera_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.ped_compra_cabecera_id_ped_compra_cabecera_seq', 4, true);


--
-- TOC entry 2473 (class 0 OID 0)
-- Dependencies: 236
-- Name: ped_compra_detalle_id_ped_compra_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.ped_compra_detalle_id_ped_compra_detalle_seq', 7, true);


--
-- TOC entry 2474 (class 0 OID 0)
-- Dependencies: 218
-- Name: pry_proyecto_id_pry_proyecto_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.pry_proyecto_id_pry_proyecto_seq', 1, false);


--
-- TOC entry 2475 (class 0 OID 0)
-- Dependencies: 220
-- Name: pry_tarea_id_pry_tarea_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.pry_tarea_id_pry_tarea_seq', 1, false);


--
-- TOC entry 2476 (class 0 OID 0)
-- Dependencies: 206
-- Name: seg_asignacion_id_seg_asignacion_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.seg_asignacion_id_seg_asignacion_seq', 6, true);


--
-- TOC entry 2477 (class 0 OID 0)
-- Dependencies: 204
-- Name: seg_modulo_id_seg_modulo_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.seg_modulo_id_seg_modulo_seq', 6, true);


--
-- TOC entry 2478 (class 0 OID 0)
-- Dependencies: 222
-- Name: seg_perfil_id_seg_perfil_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.seg_perfil_id_seg_perfil_seq', 6, true);


--
-- TOC entry 2479 (class 0 OID 0)
-- Dependencies: 202
-- Name: seg_usuario_id_seg_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.seg_usuario_id_seg_usuario_seq', 1, true);


--
-- TOC entry 2480 (class 0 OID 0)
-- Dependencies: 210
-- Name: thm_cargo_id_thm_cargo_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thm_cargo_id_thm_cargo_seq', 2, true);


--
-- TOC entry 2481 (class 0 OID 0)
-- Dependencies: 212
-- Name: thm_empleado_id_thm_empleado_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thm_empleado_id_thm_empleado_seq', 1, false);


--
-- TOC entry 2482 (class 0 OID 0)
-- Dependencies: 224
-- Name: thm_periodo_rol_id_thm_periodo_rol_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thm_periodo_rol_id_thm_periodo_rol_seq', 5, true);


--
-- TOC entry 2483 (class 0 OID 0)
-- Dependencies: 214
-- Name: thm_rol_cabecera_id_thm_rol_cabecera_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thm_rol_cabecera_id_thm_rol_cabecera_seq', 1, false);


--
-- TOC entry 2484 (class 0 OID 0)
-- Dependencies: 216
-- Name: thm_rol_detalle_id_thm_rol_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.thm_rol_detalle_id_thm_rol_detalle_seq', 1, false);


--
-- TOC entry 2237 (class 2606 OID 191285)
-- Name: aud_bitacora aud_bitacora_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.aud_bitacora
    ADD CONSTRAINT aud_bitacora_pk PRIMARY KEY (id_aud_bitacora);


--
-- TOC entry 2263 (class 2606 OID 191372)
-- Name: fact_cliente fact_cliente_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fact_cliente
    ADD CONSTRAINT fact_cliente_pk PRIMARY KEY (cedula);


--
-- TOC entry 2261 (class 2606 OID 191367)
-- Name: fact_venta_cabecera fact_venta_cabecera_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fact_venta_cabecera
    ADD CONSTRAINT fact_venta_cabecera_pk PRIMARY KEY (id_fact_venta);


--
-- TOC entry 2265 (class 2606 OID 191380)
-- Name: fact_venta_detalle fact_venta_detalle_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fact_venta_detalle
    ADD CONSTRAINT fact_venta_detalle_pk PRIMARY KEY (id_fact_venta_detalle);


--
-- TOC entry 2267 (class 2606 OID 191388)
-- Name: inv_producto inv_producto_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inv_producto
    ADD CONSTRAINT inv_producto_pk PRIMARY KEY (id_inv_producto);


--
-- TOC entry 2269 (class 2606 OID 191396)
-- Name: inv_talla inv_talla_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inv_talla
    ADD CONSTRAINT inv_talla_pk PRIMARY KEY (id_inv_talla);


--
-- TOC entry 2273 (class 2606 OID 191412)
-- Name: ped_compra_cabecera ped_compra_cabecera_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ped_compra_cabecera
    ADD CONSTRAINT ped_compra_cabecera_pk PRIMARY KEY (id_ped_compra_cabecera);


--
-- TOC entry 2271 (class 2606 OID 191404)
-- Name: ped_compra_detalle ped_compra_detalle_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ped_compra_detalle
    ADD CONSTRAINT ped_compra_detalle_pk PRIMARY KEY (id_ped_compra_detalle);


--
-- TOC entry 2275 (class 2606 OID 191417)
-- Name: ped_proveedor ped_proveedor_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ped_proveedor
    ADD CONSTRAINT ped_proveedor_pk PRIMARY KEY (ruc);


--
-- TOC entry 2251 (class 2606 OID 191329)
-- Name: pry_proyecto pry_proyecto_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pry_proyecto
    ADD CONSTRAINT pry_proyecto_pkey PRIMARY KEY (id_pry_proyecto);


--
-- TOC entry 2253 (class 2606 OID 191337)
-- Name: pry_tarea pry_tarea_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pry_tarea
    ADD CONSTRAINT pry_tarea_pkey PRIMARY KEY (id_pry_tarea);


--
-- TOC entry 2233 (class 2606 OID 191272)
-- Name: seg_asignacion seg_asignacion_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_asignacion
    ADD CONSTRAINT seg_asignacion_pk PRIMARY KEY (id_seg_asignacion);


--
-- TOC entry 2229 (class 2606 OID 191262)
-- Name: seg_modulo seg_modulo_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_modulo
    ADD CONSTRAINT seg_modulo_pk PRIMARY KEY (id_seg_modulo);


--
-- TOC entry 2255 (class 2606 OID 191345)
-- Name: seg_perfil seg_perfil_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_perfil
    ADD CONSTRAINT seg_perfil_pk PRIMARY KEY (id_seg_perfil);


--
-- TOC entry 2227 (class 2606 OID 191254)
-- Name: seg_usuario seg_usuario_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_usuario
    ADD CONSTRAINT seg_usuario_pk PRIMARY KEY (id_seg_usuario);


--
-- TOC entry 2239 (class 2606 OID 191293)
-- Name: thm_cargo thm_cargo_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_cargo
    ADD CONSTRAINT thm_cargo_pk PRIMARY KEY (id_thm_cargo);


--
-- TOC entry 2243 (class 2606 OID 191303)
-- Name: thm_empleado thm_empleado_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_empleado
    ADD CONSTRAINT thm_empleado_pk PRIMARY KEY (id_thm_empleado);


--
-- TOC entry 2257 (class 2606 OID 191353)
-- Name: thm_periodo_rol thm_periodo_rol_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_periodo_rol
    ADD CONSTRAINT thm_periodo_rol_pk PRIMARY KEY (id_thm_periodo_rol);


--
-- TOC entry 2247 (class 2606 OID 191313)
-- Name: thm_rol_cabecera thm_rol_cabecera_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_rol_cabecera
    ADD CONSTRAINT thm_rol_cabecera_pkey PRIMARY KEY (id_thm_rol_cabecera);


--
-- TOC entry 2249 (class 2606 OID 191321)
-- Name: thm_rol_detalle thm_rol_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_rol_detalle
    ADD CONSTRAINT thm_rol_detalle_pkey PRIMARY KEY (id_thm_rol_detalle);


--
-- TOC entry 2235 (class 2606 OID 191274)
-- Name: seg_asignacion uk_asignacion; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_asignacion
    ADD CONSTRAINT uk_asignacion UNIQUE (id_seg_usuario, id_seg_perfil);


--
-- TOC entry 2245 (class 2606 OID 191305)
-- Name: thm_empleado uk_empleado_usuario; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_empleado
    ADD CONSTRAINT uk_empleado_usuario UNIQUE (id_seg_usuario);


--
-- TOC entry 2231 (class 2606 OID 191264)
-- Name: seg_modulo uk_nombre_modulo; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_modulo
    ADD CONSTRAINT uk_nombre_modulo UNIQUE (nombre_modulo);


--
-- TOC entry 2259 (class 2606 OID 191355)
-- Name: thm_periodo_rol uk_periodo_rol; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_periodo_rol
    ADD CONSTRAINT uk_periodo_rol UNIQUE (nombre_periodo_rol);


--
-- TOC entry 2241 (class 2606 OID 191295)
-- Name: thm_cargo uk_thm_cargo; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_cargo
    ADD CONSTRAINT uk_thm_cargo UNIQUE (nombre_cargo);


--
-- TOC entry 2280 (class 2606 OID 191438)
-- Name: thm_rol_cabecera fk_cab_empleado; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_rol_cabecera
    ADD CONSTRAINT fk_cab_empleado FOREIGN KEY (id_thm_empleado) REFERENCES public.thm_empleado(id_thm_empleado) MATCH FULL;


--
-- TOC entry 2286 (class 2606 OID 191468)
-- Name: fact_venta_cabecera fk_cliente_venta; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fact_venta_cabecera
    ADD CONSTRAINT fk_cliente_venta FOREIGN KEY (cedula) REFERENCES public.fact_cliente(cedula) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2291 (class 2606 OID 191493)
-- Name: ped_compra_detalle fk_compra_cabecera_compra_detalle; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ped_compra_detalle
    ADD CONSTRAINT fk_compra_cabecera_compra_detalle FOREIGN KEY (id_ped_compra_cabecera) REFERENCES public.ped_compra_cabecera(id_ped_compra_cabecera) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2282 (class 2606 OID 191448)
-- Name: thm_rol_detalle fk_det_cab_rol; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_rol_detalle
    ADD CONSTRAINT fk_det_cab_rol FOREIGN KEY (id_thm_rol_cabecera) REFERENCES public.thm_rol_cabecera(id_thm_rol_cabecera) MATCH FULL;


--
-- TOC entry 2278 (class 2606 OID 191428)
-- Name: thm_empleado fk_empleado_cargo; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_empleado
    ADD CONSTRAINT fk_empleado_cargo FOREIGN KEY (id_thm_cargo) REFERENCES public.thm_cargo(id_thm_cargo) MATCH FULL;


--
-- TOC entry 2279 (class 2606 OID 191433)
-- Name: thm_empleado fk_empleado_usuario; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_empleado
    ADD CONSTRAINT fk_empleado_usuario FOREIGN KEY (id_seg_usuario) REFERENCES public.seg_usuario(id_seg_usuario) MATCH FULL;


--
-- TOC entry 2285 (class 2606 OID 191463)
-- Name: seg_perfil fk_perfil_modulo; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_perfil
    ADD CONSTRAINT fk_perfil_modulo FOREIGN KEY (id_seg_modulo) REFERENCES public.seg_modulo(id_seg_modulo) MATCH FULL;


--
-- TOC entry 2281 (class 2606 OID 191443)
-- Name: thm_rol_cabecera fk_periodo_rol_cab_rol; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.thm_rol_cabecera
    ADD CONSTRAINT fk_periodo_rol_cab_rol FOREIGN KEY (id_thm_periodo_rol) REFERENCES public.thm_periodo_rol(id_thm_periodo_rol) MATCH FULL;


--
-- TOC entry 2292 (class 2606 OID 191498)
-- Name: ped_compra_detalle fk_producto_compra_detalle; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ped_compra_detalle
    ADD CONSTRAINT fk_producto_compra_detalle FOREIGN KEY (id_inv_producto) REFERENCES public.inv_producto(id_inv_producto) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2289 (class 2606 OID 191483)
-- Name: fact_venta_detalle fk_producto_venta_detalle; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fact_venta_detalle
    ADD CONSTRAINT fk_producto_venta_detalle FOREIGN KEY (id_inv_producto) REFERENCES public.inv_producto(id_inv_producto) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2293 (class 2606 OID 191503)
-- Name: ped_compra_cabecera fk_proveedor_compra_cabecera; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ped_compra_cabecera
    ADD CONSTRAINT fk_proveedor_compra_cabecera FOREIGN KEY (ruc) REFERENCES public.ped_proveedor(ruc) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2287 (class 2606 OID 191473)
-- Name: fact_venta_cabecera fk_seg_usuario_fact_venta; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fact_venta_cabecera
    ADD CONSTRAINT fk_seg_usuario_fact_venta FOREIGN KEY (id_seg_usuario) REFERENCES public.seg_usuario(id_seg_usuario) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2290 (class 2606 OID 191488)
-- Name: inv_producto fk_talla_producto; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inv_producto
    ADD CONSTRAINT fk_talla_producto FOREIGN KEY (id_inv_talla) REFERENCES public.inv_talla(id_inv_talla) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2284 (class 2606 OID 191458)
-- Name: pry_tarea fk_tarea_proyecto; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pry_tarea
    ADD CONSTRAINT fk_tarea_proyecto FOREIGN KEY (id_pry_proyecto) REFERENCES public.pry_proyecto(id_pry_proyecto);


--
-- TOC entry 2288 (class 2606 OID 191478)
-- Name: fact_venta_detalle fk_venta_detalle; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fact_venta_detalle
    ADD CONSTRAINT fk_venta_detalle FOREIGN KEY (id_fact_venta) REFERENCES public.fact_venta_cabecera(id_fact_venta) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2283 (class 2606 OID 191453)
-- Name: pry_tarea pry_tarea_id_seg_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pry_tarea
    ADD CONSTRAINT pry_tarea_id_seg_usuario_fkey FOREIGN KEY (id_seg_usuario) REFERENCES public.seg_usuario(id_seg_usuario);


--
-- TOC entry 2276 (class 2606 OID 191418)
-- Name: seg_asignacion seg_perfil_seg_asignacion_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_asignacion
    ADD CONSTRAINT seg_perfil_seg_asignacion_fk FOREIGN KEY (id_seg_perfil) REFERENCES public.seg_perfil(id_seg_perfil);


--
-- TOC entry 2277 (class 2606 OID 191423)
-- Name: seg_asignacion seg_usuario_seg_asignacion_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seg_asignacion
    ADD CONSTRAINT seg_usuario_seg_asignacion_fk FOREIGN KEY (id_seg_usuario) REFERENCES public.seg_usuario(id_seg_usuario);


-- Completed on 2022-07-25 22:18:18

--
-- PostgreSQL database dump complete
--

