package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ped_compra_detalle database table.
 * 
 */
@Entity
@Table(name="ped_compra_detalle")
@NamedQuery(name="PedCompraDetalle.findAll", query="SELECT p FROM PedCompraDetalle p")
public class PedCompraDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ped_compra_detalle", unique=true, nullable=false)
	private Integer idPedCompraDetalle;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(name="precio_total", nullable=false, precision=7, scale=2)
	private BigDecimal precioTotal;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="id_inv_producto", nullable=false)
	private InvProducto invProducto;

	//bi-directional many-to-one association to PedCompraCabecera
	@ManyToOne
	@JoinColumn(name="id_ped_compra_cabecera", nullable=false)
	private PedCompraCabecera pedCompraCabecera;

	public PedCompraDetalle() {
	}

	public Integer getIdPedCompraDetalle() {
		return this.idPedCompraDetalle;
	}

	public void setIdPedCompraDetalle(Integer idPedCompraDetalle) {
		this.idPedCompraDetalle = idPedCompraDetalle;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecioTotal() {
		return this.precioTotal;
	}

	public void setPrecioTotal(BigDecimal precioTotal) {
		this.precioTotal = precioTotal;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

	public PedCompraCabecera getPedCompraCabecera() {
		return this.pedCompraCabecera;
	}

	public void setPedCompraCabecera(PedCompraCabecera pedCompraCabecera) {
		this.pedCompraCabecera = pedCompraCabecera;
	}

}