package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the fact_venta_cabecera database table.
 * 
 */
@Entity
@Table(name="fact_venta_cabecera")
@NamedQuery(name="FactVentaCabecera.findAll", query="SELECT f FROM FactVentaCabecera f")
public class FactVentaCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_fact_venta", unique=true, nullable=false)
	private Integer idFactVenta;

	@Column(name="costo_total", nullable=false, precision=7, scale=2)
	private BigDecimal costoTotal;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date fecha;

	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal iva;

	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal subtotal;

	//bi-directional many-to-one association to FactCliente
	@ManyToOne
	@JoinColumn(name="cedula", nullable=false)
	private FactCliente factCliente;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario", nullable=false)
	private SegUsuario segUsuario;

	//bi-directional many-to-one association to FactVentaDetalle
	@OneToMany(mappedBy="factVentaCabecera",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private List<FactVentaDetalle> factVentaDetalles;

	public FactVentaCabecera() {
	}

	public Integer getIdFactVenta() {
		return this.idFactVenta;
	}

	public void setIdFactVenta(Integer idFactVenta) {
		this.idFactVenta = idFactVenta;
	}

	public BigDecimal getCostoTotal() {
		return this.costoTotal;
	}

	public void setCostoTotal(BigDecimal costoTotal) {
		this.costoTotal = costoTotal;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getIva() {
		return this.iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public FactCliente getFactCliente() {
		return this.factCliente;
	}

	public void setFactCliente(FactCliente factCliente) {
		this.factCliente = factCliente;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

	public List<FactVentaDetalle> getFactVentaDetalles() {
		return this.factVentaDetalles;
	}

	public void setFactVentaDetalles(List<FactVentaDetalle> factVentaDetalles) {
		this.factVentaDetalles = factVentaDetalles;
	}

	public FactVentaDetalle addFactVentaDetalle(FactVentaDetalle factVentaDetalle) {
		getFactVentaDetalles().add(factVentaDetalle);
		factVentaDetalle.setFactVentaCabecera(this);

		return factVentaDetalle;
	}

	public FactVentaDetalle removeFactVentaDetalle(FactVentaDetalle factVentaDetalle) {
		getFactVentaDetalles().remove(factVentaDetalle);
		factVentaDetalle.setFactVentaCabecera(null);

		return factVentaDetalle;
	}

}