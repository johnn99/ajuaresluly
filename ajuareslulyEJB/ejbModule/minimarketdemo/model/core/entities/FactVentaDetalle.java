package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the fact_venta_detalle database table.
 * 
 */
@Entity
@Table(name="fact_venta_detalle")
@NamedQuery(name="FactVentaDetalle.findAll", query="SELECT f FROM FactVentaDetalle f")
public class FactVentaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_fact_venta_detalle", unique=true, nullable=false)
	private Integer idFactVentaDetalle;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(name="precio_total", nullable=false, precision=7, scale=2)
	private BigDecimal precioTotal;

	//bi-directional many-to-one association to FactVentaCabecera
	@ManyToOne
	@JoinColumn(name="id_fact_venta", nullable=false)
	private FactVentaCabecera factVentaCabecera;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="id_inv_producto", nullable=false)
	private InvProducto invProducto;

	public FactVentaDetalle() {
	}

	public Integer getIdFactVentaDetalle() {
		return this.idFactVentaDetalle;
	}

	public void setIdFactVentaDetalle(Integer idFactVentaDetalle) {
		this.idFactVentaDetalle = idFactVentaDetalle;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecioTotal() {
		return this.precioTotal;
	}

	public void setPrecioTotal(BigDecimal precioTotal) {
		this.precioTotal = precioTotal;
	}

	public FactVentaCabecera getFactVentaCabecera() {
		return this.factVentaCabecera;
	}

	public void setFactVentaCabecera(FactVentaCabecera factVentaCabecera) {
		this.factVentaCabecera = factVentaCabecera;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

}