package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the fact_cliente database table.
 * 
 */
@Entity
@Table(name="fact_cliente")
@NamedQuery(name="FactCliente.findAll", query="SELECT f FROM FactCliente f")
public class FactCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, length=13)
	private String cedula;

	@Column(nullable=false, length=20)
	private String apellidos;

	@Column(length=70)
	private String direccion;

	@Column(nullable=false)
	private Boolean estado;

	@Column(nullable=false, length=20)
	private String nombre;

	@Column(length=10)
	private String telefono;

	//bi-directional many-to-one association to FactVentaCabecera
	@OneToMany(mappedBy="factCliente")
	private List<FactVentaCabecera> factVentaCabeceras;

	public FactCliente() {
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<FactVentaCabecera> getFactVentaCabeceras() {
		return this.factVentaCabeceras;
	}

	public void setFactVentaCabeceras(List<FactVentaCabecera> factVentaCabeceras) {
		this.factVentaCabeceras = factVentaCabeceras;
	}

	public FactVentaCabecera addFactVentaCabecera(FactVentaCabecera factVentaCabecera) {
		getFactVentaCabeceras().add(factVentaCabecera);
		factVentaCabecera.setFactCliente(this);

		return factVentaCabecera;
	}

	public FactVentaCabecera removeFactVentaCabecera(FactVentaCabecera factVentaCabecera) {
		getFactVentaCabeceras().remove(factVentaCabecera);
		factVentaCabecera.setFactCliente(null);

		return factVentaCabecera;
	}

}