package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ped_compra_cabecera database table.
 * 
 */
@Entity
@Table(name="ped_compra_cabecera")
@NamedQuery(name="PedCompraCabecera.findAll", query="SELECT p FROM PedCompraCabecera p")
public class PedCompraCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ped_compra_cabecera", unique=true, nullable=false)
	private Integer idPedCompraCabecera;

	@Column(name="costo_total", nullable=false, precision=7, scale=2)
	private BigDecimal costoTotal;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date fecha;

	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal iva;

	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal subtotal;

	//bi-directional many-to-one association to PedProveedor
	@ManyToOne
	@JoinColumn(name="ruc", nullable=false)
	private PedProveedor pedProveedor;

	//bi-directional many-to-one association to PedCompraDetalle
	@OneToMany(mappedBy="pedCompraCabecera", cascade = CascadeType.ALL)
	private List<PedCompraDetalle> pedCompraDetalles;

	public PedCompraCabecera() {
	}

	public Integer getIdPedCompraCabecera() {
		return this.idPedCompraCabecera;
	}

	public void setIdPedCompraCabecera(Integer idPedCompraCabecera) {
		this.idPedCompraCabecera = idPedCompraCabecera;
	}

	public BigDecimal getCostoTotal() {
		return this.costoTotal;
	}

	public void setCostoTotal(BigDecimal costoTotal) {
		this.costoTotal = costoTotal;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getIva() {
		return this.iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public PedProveedor getPedProveedor() {
		return this.pedProveedor;
	}

	public void setPedProveedor(PedProveedor pedProveedor) {
		this.pedProveedor = pedProveedor;
	}

	public List<PedCompraDetalle> getPedCompraDetalles() {
		return this.pedCompraDetalles;
	}

	public void setPedCompraDetalles(List<PedCompraDetalle> pedCompraDetalles) {
		this.pedCompraDetalles = pedCompraDetalles;
	}

	public PedCompraDetalle addPedCompraDetalle(PedCompraDetalle pedCompraDetalle) {
		getPedCompraDetalles().add(pedCompraDetalle);
		pedCompraDetalle.setPedCompraCabecera(this);

		return pedCompraDetalle;
	}

	public PedCompraDetalle removePedCompraDetalle(PedCompraDetalle pedCompraDetalle) {
		getPedCompraDetalles().remove(pedCompraDetalle);
		pedCompraDetalle.setPedCompraCabecera(null);

		return pedCompraDetalle;
	}

}