package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the inv_producto database table.
 * 
 */
@Entity
@Table(name="inv_producto")
@NamedQuery(name="InvProducto.findAll", query="SELECT i FROM InvProducto i")
public class InvProducto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//producto 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_inv_producto", unique=true, nullable=false)
	private Integer idInvProducto;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(name="descripcion_producto", nullable=false, length=100)
	private String descripcionProducto;

	@Column(nullable=false)
	private Boolean estado;

	@Column(name="nombre_producto", nullable=false, length=50)
	private String nombreProducto;

	@Column(nullable=false, precision=7, scale=2)
	private BigDecimal precio;

	@Column(name="pv_publico", nullable=false, precision=7, scale=2)
	private BigDecimal pvPublico;

	//bi-directional many-to-one association to FactVentaDetalle
	@OneToMany(mappedBy="invProducto")
	private List<FactVentaDetalle> factVentaDetalles;

	//bi-directional many-to-one association to InvTalla
	@ManyToOne
	@JoinColumn(name="id_inv_talla", nullable=false)
	private InvTalla invTalla;

	//bi-directional many-to-one association to PedCompraDetalle
	@OneToMany(mappedBy="invProducto")
	private List<PedCompraDetalle> pedCompraDetalles;

	public InvProducto() {
	}

	public Integer getIdInvProducto() {
		return this.idInvProducto;
	}

	public void setIdInvProducto(Integer idInvProducto) {
		this.idInvProducto = idInvProducto;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcionProducto() {
		return this.descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getNombreProducto() {
		return this.nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public BigDecimal getPrecio() {
		return this.precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public BigDecimal getPvPublico() {
		return this.pvPublico;
	}

	public void setPvPublico(BigDecimal pvPublico) {
		this.pvPublico = pvPublico;
	}

	public List<FactVentaDetalle> getFactVentaDetalles() {
		return this.factVentaDetalles;
	}

	public void setFactVentaDetalles(List<FactVentaDetalle> factVentaDetalles) {
		this.factVentaDetalles = factVentaDetalles;
	}

	public FactVentaDetalle addFactVentaDetalle(FactVentaDetalle factVentaDetalle) {
		getFactVentaDetalles().add(factVentaDetalle);
		factVentaDetalle.setInvProducto(this);

		return factVentaDetalle;
	}

	public FactVentaDetalle removeFactVentaDetalle(FactVentaDetalle factVentaDetalle) {
		getFactVentaDetalles().remove(factVentaDetalle);
		factVentaDetalle.setInvProducto(null);

		return factVentaDetalle;
	}

	public InvTalla getInvTalla() {
		return this.invTalla;
	}

	public void setInvTalla(InvTalla invTalla) {
		this.invTalla = invTalla;
	}

	public List<PedCompraDetalle> getPedCompraDetalles() {
		return this.pedCompraDetalles;
	}

	public void setPedCompraDetalles(List<PedCompraDetalle> pedCompraDetalles) {
		this.pedCompraDetalles = pedCompraDetalles;
	}

	public PedCompraDetalle addPedCompraDetalle(PedCompraDetalle pedCompraDetalle) {
		getPedCompraDetalles().add(pedCompraDetalle);
		pedCompraDetalle.setInvProducto(this);

		return pedCompraDetalle;
	}

	public PedCompraDetalle removePedCompraDetalle(PedCompraDetalle pedCompraDetalle) {
		getPedCompraDetalles().remove(pedCompraDetalle);
		pedCompraDetalle.setInvProducto(null);

		return pedCompraDetalle;
	}

}