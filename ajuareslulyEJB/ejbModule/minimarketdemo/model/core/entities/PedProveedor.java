package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ped_proveedor database table.
 * 
 */
@Entity
@Table(name="ped_proveedor")
@NamedQuery(name="PedProveedor.findAll", query="SELECT p FROM PedProveedor p")
public class PedProveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, length=13)
	private String ruc;

	@Column(nullable=false, length=80)
	private String direccion;

	@Column(nullable=false, length=30)
	private String empresa;

	@Column(nullable=false)
	private Boolean estado;

	@Column(nullable=false, length=10)
	private String telefono;

	//bi-directional many-to-one association to PedCompraCabecera
	@OneToMany(mappedBy="pedProveedor")
	private List<PedCompraCabecera> pedCompraCabeceras;

	public PedProveedor() {
	}

	public String getRuc() {
		return this.ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<PedCompraCabecera> getPedCompraCabeceras() {
		return this.pedCompraCabeceras;
	}

	public void setPedCompraCabeceras(List<PedCompraCabecera> pedCompraCabeceras) {
		this.pedCompraCabeceras = pedCompraCabeceras;
	}

	public PedCompraCabecera addPedCompraCabecera(PedCompraCabecera pedCompraCabecera) {
		getPedCompraCabeceras().add(pedCompraCabecera);
		pedCompraCabecera.setPedProveedor(this);

		return pedCompraCabecera;
	}

	public PedCompraCabecera removePedCompraCabecera(PedCompraCabecera pedCompraCabecera) {
		getPedCompraCabeceras().remove(pedCompraCabecera);
		pedCompraCabecera.setPedProveedor(null);

		return pedCompraCabecera;
	}

}