package ajuaresluly.model.venta.dtos;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class DTOVentaDetalle
 */
@Stateless
@LocalBean
public class DTOVentaDetalle {
	
	String descripcion;
	int cantidad;
	double subtotal;
	double iva;
	

    /**
     * Default constructor. 
     */
    public DTOVentaDetalle() {
        // TODO Auto-generated constructor stub
    }

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}
    
    

}
