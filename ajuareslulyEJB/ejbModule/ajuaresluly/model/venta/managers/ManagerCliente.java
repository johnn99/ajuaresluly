package ajuaresluly.model.venta.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.FactCliente;
import minimarketdemo.model.core.managers.ManagerDAO;

/**
 * Session Bean implementation class ManagerCliente
 */
@Stateless
@LocalBean
public class ManagerCliente {

    @PersistenceContext
    private EntityManager em;
    
    @EJB
    private ManagerDAO mDAO;
    
    public ManagerCliente() {
       
    }
    
    public List<FactCliente> findAllCliente(){
    	Query q=em.createQuery("select o from FactCliente o", FactCliente.class);
    	return q.getResultList();
    }
    
    public void guardarCliente(String cedula, String nombre, String apellidos, String telefono, String direccion, boolean estado) {
    	FactCliente cliente= new FactCliente();
    	cliente.setCedula(cedula);
    	cliente.setNombre(nombre);
    	cliente.setApellidos(apellidos);
    	cliente.setTelefono(telefono);
    	cliente.setDireccion(direccion);
    	cliente.setEstado(true);
    	em.persist(cliente);
    }
    
    public FactCliente findClienteByCedula(String cedula) {
    	return em.find(FactCliente.class, cedula);
    }
    
    public void eliminarCliente(String cedula) {
    	FactCliente cliente= findClienteByCedula(cedula);
    	em.remove(cliente);
    }
    
    public void actualizarCliente(FactCliente cliente) throws Exception {
    	FactCliente c= findClienteByCedula(cliente.getCedula());
    	c.setCedula(cliente.getCedula());
    	c.setNombre(cliente.getNombre());
    	c.setApellidos(cliente.getApellidos());
    	c.setTelefono(cliente.getTelefono());
    	c.setDireccion(cliente.getDireccion());
    	em.merge(c);
		
    }
}
