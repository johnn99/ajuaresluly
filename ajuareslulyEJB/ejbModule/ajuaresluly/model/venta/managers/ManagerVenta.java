package ajuaresluly.model.venta.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.FactCliente;
import minimarketdemo.model.core.entities.FactVentaCabecera;
import minimarketdemo.model.core.entities.FactVentaDetalle;
import minimarketdemo.model.core.entities.InvProducto;
import minimarketdemo.model.core.entities.SegUsuario;
import minimarketdemo.model.core.managers.ManagerDAO;

/**
 * Session Bean implementation class ManagerVenta
 */
@Stateless
@LocalBean
public class ManagerVenta {

	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private ManagerDAO mDAO;
    /**
     * Default constructor. 
     */
    public ManagerVenta() {
        // TODO Auto-generated constructor stub
    }
    
    public List<InvProducto> findAllProductos(){
    	String consulta = "select p from InvProducto p";
    	Query q = em.createQuery(consulta, InvProducto.class);
    	return q.getResultList();
    }
    
    //metodo listar
    public List<FactCliente> findAllClientes(){
    	String consulta = "select c from FactCliente c";
    	Query q = em.createQuery(consulta, FactCliente.class);
    	return q.getResultList();
    }
    
    public List<SegUsuario> findAllUsuarios(){
    	return mDAO.findAll(SegUsuario.class, "apellidos");
    }

    public List<FactVentaCabecera> findAllVentas(){
    	String consulta = "select v from FactVentaCabecera v";
    	Query q = em.createQuery(consulta, FactVentaCabecera.class);
    	return q.getResultList();
    }
    
    public SegUsuario findUserById(int idUsuario) {
    	SegUsuario usuario = em.find(SegUsuario.class, idUsuario);
    	return usuario;
    }
    
    public List<FactVentaDetalle> agregarDetalle (List<FactVentaDetalle> listaDetalles, InvProducto ProductoSeleccionado, int cantidad) throws Exception {
    	
    	if(listaDetalles == null)
    		listaDetalles = new ArrayList<FactVentaDetalle>();
    	FactVentaDetalle detalle = new FactVentaDetalle();
    	InvProducto Producto = new InvProducto();
    	Producto = findProductoById(ProductoSeleccionado.getIdInvProducto());
    	
    	detalle.setCantidad(cantidad);
    	detalle.setPrecioTotal(new BigDecimal(cantidad * Producto.getPvPublico().doubleValue()));
    	detalle.setInvProducto(Producto);
    	
    	listaDetalles.add(detalle);
    	
    	return listaDetalles;
    }
    
    public void actualizarStock(List<FactVentaDetalle> listDetalles) throws Exception {
    	
    	int idProducto = 0;
    	int cantidad =0;
    	int stock = 0;
    	
    	for(int i=0; i<listDetalles.size();i++) {
    		InvProducto produ = new InvProducto();
    		idProducto = listDetalles.get(i).getInvProducto().getIdInvProducto();
    		cantidad = listDetalles.get(i).getCantidad();
    		
    		produ = em.find(InvProducto.class, idProducto);
    		stock = produ.getCantidad() - cantidad;
    		produ.setCantidad(stock);
    		
    		em.merge(produ);  		
    	}
    	
    }
    
    public InvProducto findProductoById (int idProducto) throws Exception {
    	InvProducto producto = em.find(InvProducto.class, idProducto);
    	return producto;
    }
    
    public double calcularSubtotalVenta (List<FactVentaDetalle> listaDetalles) {
    	double subtotal = 0;
    	for(FactVentaDetalle d:listaDetalles)
    		subtotal+=d.getPrecioTotal().doubleValue();
    	return subtotal;
    }
    
    public double calcularIvaVenta(double subtotal) {
    	double iva = 0.12*subtotal;
    	return iva;
    }
    
    public double totalDetalleVenta(double subtotal, double iva) {
    	double total = subtotal + iva;
    	return total;
    }
    
    public void guardarVenta(String cedula, Date fecha, int idSegUsuario, 
    		double subtotal, double iva, double costoTotal, List<FactVentaDetalle> listaDetalles) throws Exception {
    	
    	//validaciones
    	if(listaDetalles == null || listaDetalles.size()==0)
    		throw new Exception("Debe haber al menos un detalle de venta.");
    	
    	//crear la cabecera
    	FactVentaCabecera cabecera = new FactVentaCabecera();
    	FactCliente cliente = em.find(FactCliente.class, cedula);
    	SegUsuario usuario = em.find(SegUsuario.class, idSegUsuario);
    	
    	cabecera.setFactCliente(cliente);
    	cabecera.setFecha(fecha);
    	cabecera.setSegUsuario(usuario);
    	cabecera.setSubtotal(new BigDecimal(subtotal));
    	cabecera.setIva(new BigDecimal(iva));
    	cabecera.setCostoTotal(new BigDecimal(costoTotal));
    	cabecera.setFactVentaDetalles(listaDetalles);
    	
    	for(FactVentaDetalle det: listaDetalles) {
    		det.setFactVentaCabecera(cabecera);
    	}
    	//guardar el maestro detalle
    	em.persist(cabecera);
    	
    }
    
}
