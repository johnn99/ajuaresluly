package ajuaresluly.model.inventario.productos.managers;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.InvProducto;
import minimarketdemo.model.core.entities.InvTalla;

@Stateless
@LocalBean
public class ManagerProducto {
	@PersistenceContext
	private EntityManager em;

	public ManagerProducto() {

	}

	public List<InvTalla> findAllTallaProducto() {
		String consulta = "select t from InvTalla t";
		Query q = em.createQuery(consulta, InvTalla.class);
		return q.getResultList();
	}

	public List<InvProducto> findAllTotalProducto() {
		return em.createNamedQuery("InvProducto.findAll", InvProducto.class).getResultList();
	}

	public List<InvProducto> findAllProducto() {
		String consultar = "SELECT i FROM InvProducto i order by i.idInvProducto";
		Query q = em.createQuery(consultar, InvProducto.class);
		return q.getResultList();
	}

	public List<InvProducto> findAllProductosEnStock() {
		Query q = em.createQuery("select i from InvProducto where i.cantidad > 0 order by i.idInvProducto",
				InvProducto.class);
		return q.getResultList();
	}

	public InvProducto findProductoById(int idProducto) {
		return em.find(InvProducto.class, idProducto);
	}

	public BigDecimal calcularPrecioVentaPublico(BigDecimal precio) {
		double e = (precio).doubleValue() * ((100 + 12) / 100) * (1.12);
		return new BigDecimal(e).round(new MathContext(5));
	}

	public void  eliminarProducto(int id_inv_producto) {
		
		InvProducto producto = findProductoById(id_inv_producto);
		em.remove(producto); 
		
	}

	public void actualizarProducto(InvProducto producto, int IdTalla) throws Exception {

		InvProducto produ = findProductoById(producto.getIdInvProducto());
		InvTalla talla = em.find(InvTalla.class, IdTalla);

		produ.setNombreProducto(producto.getNombreProducto());
		produ.setDescripcionProducto(producto.getDescripcionProducto());
		produ.setPrecio(producto.getPrecio());
		produ.setCantidad(producto.getCantidad());
		produ.setPvPublico(producto.getPvPublico());
		produ.setInvTalla(talla);
		em.merge(produ);
	}

	public void ingresarNuevoProducto(String nombre, String descripcion, double precio, int cantidad, double pvPublico, int idTalla) {
		
		InvTalla talla = em.find(InvTalla.class, idTalla);
		InvProducto producto = new InvProducto();
		producto.setNombreProducto(nombre);
		producto.setDescripcionProducto(descripcion);
		producto.setPrecio(new BigDecimal(precio));
		producto.setCantidad(cantidad);
		producto.setPvPublico(new BigDecimal(pvPublico));
		producto.setEstado(true);
		producto.setInvTalla(talla);
		
		em.persist(producto);
		
	}
	
	
	/*
	public void ingresarNuevoProducto(InvProducto producto, int idTalla) {
		
		InvTalla talla = em.find(InvTalla.class, idTalla);
		InvProducto produ = new InvProducto();
		
		produ.setNombreProducto(producto.getNombreProducto());
		produ.setDescripcionProducto(producto.getDescripcionProducto());
		produ.setPrecio(producto.getPrecio());
		produ.setCantidad(producto.getCantidad());
		produ.setPvPublico(producto.getPvPublico());
		produ.setEstado(true);
		produ.setInvTalla(talla);
		
		em.persist(produ);
		
	}
	*/

}
