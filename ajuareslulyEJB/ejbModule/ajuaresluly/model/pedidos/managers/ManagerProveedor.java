package ajuaresluly.model.pedidos.managers;

import java.security.Provider;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.PedProveedor;

/**
 * Session Bean implementation class ManagerProveedor
 */
@Stateless
@LocalBean
public class ManagerProveedor {

	@PersistenceContext

	private EntityManager em;

	public ManagerProveedor() {

	}

	public List<PedProveedor> findAllProveedor() {
		Query q = em.createQuery("select o from PedProveedor o", PedProveedor.class);
		return q.getResultList();

	}

	public void guardarProveedor(String ruc, String nombreEmpresa, String telefono, String direccion, boolean estado) {
		PedProveedor proveedor = new PedProveedor();
		proveedor.setRuc(ruc);
		proveedor.setEmpresa(nombreEmpresa);
		proveedor.setTelefono(telefono);
		proveedor.setDireccion(direccion);
		proveedor.setEstado(true);
		em.persist(proveedor);
	}
	
	public PedProveedor findProveedorByRuc(String ruc) {
		return em.find(PedProveedor.class, ruc);
	}
	
	public void eliminarProveedor(String ruc) {
		PedProveedor proveedor=findProveedorByRuc(ruc);
		em.remove(proveedor);
	}
	
	public void actualizarProveedor(PedProveedor proveedor) {
		PedProveedor p= findProveedorByRuc(proveedor.getRuc());
		p.setRuc(proveedor.getRuc());
		p.setEmpresa(proveedor.getEmpresa());
		p.setDireccion(proveedor.getDireccion());
		p.setTelefono(proveedor.getTelefono());
		em.merge(p);
	}
}
