package ajuaresluly.model.pedidos.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import minimarketdemo.model.core.entities.InvProducto;
import minimarketdemo.model.core.entities.PedCompraCabecera;
import minimarketdemo.model.core.entities.PedCompraDetalle;
import minimarketdemo.model.core.entities.PedProveedor;

/**
 * Session Bean implementation class ManagerPedido
 */
@Stateless
@LocalBean
public class ManagerPedido {
	
	@PersistenceContext
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public ManagerPedido() {
        // TODO Auto-generated constructor stub
    }
    
    public List<InvProducto> findAllProductos(){
    	String consulta = "select p from InvProducto p";
    	Query q = em.createQuery(consulta, InvProducto.class);
    	return q.getResultList();
    }
    
    public List<PedProveedor> findAllProveedores(){
    	String consulta = "select r from PedProveedor r";
    	Query q = em.createQuery(consulta, PedProveedor.class);
    	return q.getResultList();
    }
    
    public List<PedCompraCabecera> findAllPedidos(){
    	String consulta = "select p from PedCompraCabecera p";
    	Query q = em.createQuery(consulta, PedCompraCabecera.class);
    	return q.getResultList();
    }
    
    public List<PedCompraDetalle> agregarDetalle (List<PedCompraDetalle> listaDetalles, InvProducto ProductoSeleccionado,
    		int cantidad) throws Exception {
    	
    	if(listaDetalles == null)
    		listaDetalles = new ArrayList<PedCompraDetalle>();
    	
    	PedCompraDetalle detalle = new PedCompraDetalle();
    	InvProducto producto = new InvProducto();
    	producto = findProductoById(ProductoSeleccionado.getIdInvProducto());
    	
    	detalle.setCantidad(cantidad);
    	detalle.setPrecioTotal(new BigDecimal(cantidad * producto.getPrecio().doubleValue()));
    	detalle.setInvProducto(producto);
    	
    	listaDetalles.add(detalle);
    	
    	return listaDetalles;
    }
    
    public void actualizarStock(List<PedCompraDetalle> listaDetalles) {
    	int idProducto = 0;
    	int cantidad = 0;
    	int stock = 0;
    	
    	for(int i=0; i<listaDetalles.size();i++) {
    		InvProducto produ = new InvProducto();
    		idProducto = listaDetalles.get(i).getInvProducto().getIdInvProducto();
    		cantidad = listaDetalles.get(i).getCantidad();
    		
    		produ = em.find(InvProducto.class, idProducto);
    		stock = produ.getCantidad() + cantidad;
    		produ.setCantidad(stock);
    		
    		em.merge(produ);
    	}
    }
    
    
    public InvProducto findProductoById (int idProducto) throws Exception {
    	InvProducto producto = em.find(InvProducto.class, idProducto);
    	return producto;
    }
    
    public double calcularSubtotalPedido (List<PedCompraDetalle> listaDetalles) {
    	double subtotal = 0;
    	for(PedCompraDetalle d: listaDetalles)
    		subtotal+=d.getPrecioTotal().doubleValue();
    	
    	return subtotal;
    }
    
    public double calcularIvaPedido(double subtotal) {
    	double iva = 0.12*subtotal;
    	return iva;
    }
    
    public double totalDetallePedido(double subtotal, double iva) {
    	double total = subtotal + iva;
    	return total;
    }

    public void guardarPedido(String ruc, Date fecha, double subtotal,
    		double iva, double costoTotal, List<PedCompraDetalle> listaDetalles) throws Exception {
    	
    	//validaciones
    	if(listaDetalles == null || listaDetalles.size()==0)
    		throw new Exception("Debe haber al menos un detalle de pedido.");
    	
    	//crear la cabecera
    	PedCompraCabecera cabecera = new PedCompraCabecera();
    	PedProveedor proveedor = em.find(PedProveedor.class, ruc);
    	
    	cabecera.setPedProveedor(proveedor);
    	cabecera.setFecha(fecha);
    	cabecera.setSubtotal(new BigDecimal(subtotal));
    	cabecera.setIva(new BigDecimal(iva));
    	cabecera.setCostoTotal(new BigDecimal(costoTotal));
    	cabecera.setPedCompraDetalles(listaDetalles);
    	
    	for(PedCompraDetalle det: listaDetalles) {
    		det.setPedCompraCabecera(cabecera);
    	}
    	
    	//guardar maestro detalle
    	em.persist(cabecera);
    	
    }
    
    public List<PedCompraDetalle> findDetallleByCabeceraId(PedCompraCabecera cabeceraSeleccionada) throws Exception{
    	
    	PedCompraCabecera cabecera = new PedCompraCabecera();
    	cabecera = em.find(PedCompraCabecera.class, cabeceraSeleccionada.getIdPedCompraCabecera());
    	int idCabecera = cabecera.getIdPedCompraCabecera();
    	
    	String consulta = "select c from PedCompraDetalle c where c.pedCompraCabecera= ?1";
    	Query q = em.createQuery(consulta).setParameter(1, idCabecera);
    	return q.getResultList();
    }
}
